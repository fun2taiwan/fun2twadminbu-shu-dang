/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
	imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates: [ {
		title: '食品規格樣板',
		image: 'DSC06644-250x250.JPG',
		description: '營養標示 主要原料 保存期限 重量 有效期限 保存方式',
		html: '<table border="0" style="width:100%">'+
'			<tbody><tr>'+
'				<td valign="top">'+
'                   <table width="350px">'+
'                      <tbody><tr>'+
'                        <td width="70px">'+
'                          <h2>數量：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                  </td>'+
'                      </tr><tr>'+
'                        <td valign="top">'+
'                          <h2>主要原料：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                 </td>'+
'                      </tr>'+                    
'                      <tr>'+
'                        <td>'+
'                          <h2>保存期限：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                </td>'+
'                      </tr>'+                    
'                      <tr>'+
'                        <td>'+
'                          <h2>重量：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                 </td>'+
'                      </tr>'+                
'                      <tr>'+
'                        <td>'+
'                          <h2>有效期限：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                 </td>'+
'                      </tr>'+                   
'                      <tr>'+
'                        <td valign="top">'+
'                          <h2>保存方式：</h2>'+
'                        </td>'+
'                        <td>'+
'                                                  </td>'+
'                      </tr>'+                   
'                    </tbody></table>'+
'				</td>'+
'				<td align="right">'+
'					<table id="nutritionindication">'+
'					<tbody><tr style="border: 1px solid #333333;">'+
'					<td style="border: 1px solid #333333; padding:5px;" colspan="2" align="center"><b>營養標示</b></td>'+
'					</tr>'+
'					<tr style="border: 1px solid #333333;">'+
'					<td style="border: 1px solid #333333; padding:5px;" colspan="2" align="center">每100公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="left" class="nutrition1">熱量</td>'+
'						<td align="right" class="nutrition2">大卡</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="left" class="nutrition1">蛋白質</td>'+
'						<td align="right" class="nutrition2">公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="left" class="nutrition1">脂肪</td>'+
'						<td align="right" class="nutrition2">公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="right" class="nutrition1">	飽和脂肪</td>'+
'						<td align="right" class="nutrition2">公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="right" class="nutrition1">	反式脂肪</td>'+
'						<td align="right" class="nutrition2">0公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="left" class="nutrition1">碳水化合物</td>'+
'						<td align="right" class="nutrition2">公克</td>'+
'					</tr>'+
'					<tr>'+
'						<td align="left" class="nutrition1">納</td>'+
'						<td align="right" style="border: 1px 1px 1px 0 solid #333333; padding:5px;">毫克</td>'+
'					</tr>'+
'					</tbody></table>'+
'				</td>'+
'			</tr>'+
'		</tbody></table>'
	},
	{
		title: '表格樣板',
		image: 'template2.gif',
		description: '表格樣板',
		html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0">' +
			'<tr>' +
				'<td style="width:50%">' +
					'<h3>Title 1</h3>' +
				'</td>' +
				'<td></td>' +
				'<td style="width:50%">' +
					'<h3>Title 2</h3>' +
				'</td>' +
			'</tr>' +
			'<tr>' +
				'<td>' +
					'Text 1' +
				'</td>' +
				'<td></td>' +
				'<td>' +
					'Text 2' +
				'</td>' +
			'</tr>' +
			'</table>' +
			'<p>' +
			'More text goes here.' +
			'</p>'
	} ]
} );
