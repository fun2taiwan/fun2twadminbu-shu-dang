ame">
      <summary>Represents the name of the configuration section for Razor pages.</summary>
    </member>
    <member name="T:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup">
      <summary>Provides configuration system support for the system.web.webPages.razor configuration section.</summary>
    </member>
    <member name="M:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup" /> class.</summary>
    </member>
    <member name="F:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup.GroupName">
      <summary>Represents the name of the configuration section for Razor Web section. Contains the static, read-only string "system.web.webPages.razor".</summary>
    </member>
    <member name="P:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup.Host">
      <summary>Gets or sets the host value for system.web.webPages.razor section group.</summary>
      <returns>The host value.</returns>
    </member>
    <member name="P:System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup.Pages">
      <summary>Gets or sets the value of the pages element for the system.web.webPages.razor section.</summary>
      <returns>The pages element value.</returns>
    </member>
  </members>
</doc>                                                                                                                                                  } 
 

//            ViewBag.Subject = Subject;
            return View(planimages.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }



        

        //
        // GET: /TravelPlanImage/Details/5

        public ActionResult Details(int id = 0)
        {
            TravelPlanImage travelplanimage = _db.PlanImages.Find(id);
            if (travelplanimage == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(travelplanimage);
        }

        //
        // GET: /TravelPlanImage/Create

        public ActionResult Create()
        {
            ViewBag.TravelPlanId = new SelectList(_db.TravelPlans.OrderBy(p=>p.ListNum), "Id", "Subject");
            return View();
        }

        //
        // POST: /TravelPlanImage/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(TravelPlanImage travelplanimage )
        {
            if (ModelState.IsValid)
            {

                _db.PlanImages.Add(travelplanimage);
                travelplanimage.Create(_db,_db.PlanImages);
                return RedirectToAction("Index");
            }

            ViewBag.TravelPlanId = new SelectList(_db.TravelPlans.OrderBy(p=>p.ListNum), "Id", "Subject", travelplanimage.TravelPlanId);
            return View(travelplanimage);
        }

        //
        // GET: /TravelPlanImage/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TravelPlanImage travelplanimage = _db.PlanImages.Find(id);
            if (travelplanimage == null)
            {
                return HttpNotFound();
            }
            ViewBag.TravelPlanId = new SelectList(_db.TravelPlans.OrderBy(p=>p.ListNum), "Id", "Subject", travelplanimage.TravelPlanId);
            return View(travelplanimage);
        }

        //
        // POST: /TravelPlanImage/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(TravelPlanImage travelplanimage)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(travelplanimage).State = EntityState.Modified;
                travelplanimage.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            ViewBag.TravelPlanId = new SelectList(_db.TravelPlans.OrderBy(p=>p.ListNum), "Id", "Subject", travelplanimage.TravelPlanId);
          GIF89a[� �  �����ynsi���J]aGeRabrukAj��������q��������sVcc��w*Lc#Id���hng��|�������;��ꈅuG[a'JdFf}}o���@k                                                                                          !�     ,    [�  �@�pH,�Ȥr�l:�ШtJ�Z�جv��z��xL.���z�n[5�$D�����~���������������w_�����������Z��������}W
�������!
T ������| Q����ΧO �������M������M	����	K����I�����I� 
�� 	���cp �#�CЍ��3:�`$�Ə 0!�ɓ� )���KN���I��̚8s
����O�<<
�t�Q�E�*E�t�ӏM�J�u�ՁU�j՗u��x]��Ev�YmeϪ}�v�[bm���w��Zu��]�w�_S}�
�x�a�E+fUx��A�K�y��<�/k��y��Ξ'��x4�ŦON�z0��_��+{��ڶ����v7ﵾ�.|,��_�#ߪ|���ΧB��t:��֯ͮ}(��?��'�x����E�O_z={��߯�/�5����㧭�����`��H p8\�
�`��=!sN�\�J�a��m��!v~�]�"zGb�ᝈ"yD���x.c���Hc>6�8O�:��b�.��:BI֏F�Td��,�$ZH>���R^Ce�lE�%UZn�Е^.f�ÌIf0f�IW�j�f���	'^lΉ��v��g�|��'<{��J���g�G��hK�.�I��fi��LJ)b�^
ա�Z�i�Yf
�D��jH���j�;}�j����f���)j�2Ҋk���J签��k�zKl���c�ʮ�j�u<m�B[m��*�����m������Zn��ʚ����n����Zo��j���R�o��:�#Zp�+MOLE�0<t��:#����d`�C�C�u�H�#28!q������;,_SO���L;L�l31.3A���p� �@@F�RAEQ��4-�X��S��,YP� KYs�|�A t�" n�-��t�m��x���|� ;                                                                                                                                                                                                                                                                                                                                                                   urce reconnecting due to the server connection ending."),h.reconnect(e)):(e.log("EventSource error."),l.triggerHandler(u.onError,[i._.transportError(i.resources.eventSourceError,e.transport,n)]))}},!1)},reconnect:function(n){r.reconnect(n,this.name)},lostConnection:function(n){this.reconnect(n)},send:function(n,t){r.ajaxSend(n,t)},stop:function(n){r.clearReconnectTimeout(n);n&&n.eventSource&&(n.log("EventSource calling close()."),n.eventSource.close(),n.eventSource=null,delete n.eventSource)},abort:function(n,t){r.ajaxAbort(n,t)}}}(window.jQuery,window),function(n,t){"use strict";var r=n.signalR,f=n.signalR.events,e=n.signalR.changeState,i=r.transports._logic,u=function(){var u=null,f=1e3,i=0;return{prevent:function(){r._.ieVersion<=8&&(i===0&&(u=t.setInterval(function(){var t=n("<iframe style='position:absolute;top:0;left:0;width:0;height:0;visibility:hidden;' src=''><\/iframe>");n("body").append(t);t.remove();t=null},f)),i++)},cancel:function(){i===1&&t.clearInterval(u);i>0&&i--}}}();r.transports.foreverFrame={name:"foreverFrame",supportsKeepAlive:!0,iframeClearThreshold:50,start:function(r,f,e){var c=this,s=i.foreverFrame.count+=1,h,o=n("<iframe data-signalr-connection-id='"+r.id+"' style='position:absolute;top:0;left:0;width:0;height:0;visibility:hidden;' src=''><\/iframe>");if(t.EventSource){e&&(r.log("This browser supports SSE, skipping Forever Frame."),e());return}u.prevent();h=i.getUrl(r,this.name);h+="&frameId="+s;n("body").append(o);o.prop("src",h);i.foreverFrame.connections[s]=r;r.log("Binding to iframe's readystatechange event.");o.bind("readystatechange",function(){n.inArray(this.readyState,["loaded","complete"])>=0&&(r.log("Forever frame iframe readyState changed to "+this.readyState+", reconnecting."),c.reconnect(r))});r.frame=o[0];r.frameId=s;f&&(r.onSuccess=function(){r.log("Iframe transport started.");f();delete r.onSuccess})},reconnect:function(n){var r=this;t.setTimeout(function(){if(n.frame&&i.ensureReconnectingState(n)){var u=n.frame,t=i.getUrl(n,r.name,!0)+"&frameId="+n.frameId;n.log("Updating iframe src to '"+t+"'.");u.src=t}},n.reconnectDelay)},lostConnection:function(n){this.reconnect(n)},send:function(n,t){i.ajaxSend(n,t)},receive:function(t,u){var f;i.processMessages(t,u,t.onSuccess);t.state===n.signalR.connectionState.connected&&(t.frameMessageCount=(t.frameMessageCount||0)+1,t.frameMessageCount>r.transports.foreverFrame.iframeClearThreshold&&(t.frameMessageCount=0,f=t.frame.contentWindow||t.frame.contentDocument,f&&f.document&&n("body",f.document).empty()))},stop:function(t){var r=null<?xml version="1.0"?>
<package xmlns="http://schemas.microsoft.com/packaging/2011/08/nuspec.xsd">
  <metadata>
    <id>MvcPaging</id>
    <version>2.1.5</version>
    <title>MvcPaging</title>
    <authors>Martijn Boland, Bart Lenaerts, Rajeesh CV</authors>
    <owners>Martijn Boland, Bart Lenaerts, Rajeesh CV</owners>
    <licenseUrl>https://github.com/martijnboland/MvcPaging/wiki/License</licenseUrl>
    <projectUrl>https://github.com/martijnboland/MvcPaging</projectUrl>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>A Paging library for ASP.NET MVC.</description>
    <summary />
    <releaseNotes>2.1.5 (2016-05-19)
- A