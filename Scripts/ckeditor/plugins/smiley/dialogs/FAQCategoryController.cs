using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Fun2twAdmin.Filters;
using Fun2twModels.Models;

namespace Fun2twAdmin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class NavigationController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            
            return View(_db.Navigations.OrderByDescending(p=>p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }



        

        //
        // GET: /Navigation/Details/5

        public ActionResult Details(int id = 0)
        {
            Navigation navigation = _db.Navigations.Find(id);
            if (navigation == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(navigation);
        }

        //
        // GET: /Navigation/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Navigation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Navigation navigation )
        {
            if (ModelState.IsValid)
            {

                _db.Navigations.Add(navigation);
                navigation.Create(_db,_db.Navigations);
                return RedirectToAction("Index");
            }

            return View(navigation);
        }

        //
        // GET: /Navigation/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Navigation navigation = _db.Navigations.Find(id);
            if (navigation == null)
            {
                return HttpNotFound();
            }
            return View(navigation);
        }

        //
        // POST: /Navigation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Navigation navigation)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(navigation).State = EntityState.Modified;
                navigation.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(navigation);
        }

        //
        // GET: /Navigation/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Navigation navigation = _db.Navigations.Find(id);
            if (navigation == null)
            {
                return HttpNotFound();
            }
            return View(navigation);
        }

        //
        // POST: /Navigation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Navigation navigation = _db.Navigations.Find(id);
            _db.Navigations.Remove(navigation);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}

r	 poy  
��-r ~[  -=r p�  (d  
��  	(z  
�	(z  
�	({  
(|  
�[  + ~[  {}  
~[  (~  
r% po  
&(l  
8  (� o�  
 o�  
o�  
(�  
r5 po�  
rW p(�    �  (�  
 {  oc  
o�  
&
{  oc  
(  +�-T {  oc  
�  (d  
r[ p(e  
�f  
(g  
t�  (h  
�=  �(  +(  +
 Xoq  
 {  {  oc  
(�  
o�  
 r/ p(s  
+
(l  
+ *0 D      {  oc  
�  �u  �op  

��	-
 (�  
+
(l  
+ *:(  
}�  
* 0      {�  

+���� JFIF  : :  �� C 		
 $.' ",#(7),01444'9=82<.342�� C			2!!22222222222222222222222222222222222222222222222222��  q �" ��           	
�� �   } !1AQa"q2���#B��R��$3br�	
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������        	
�� �  w !1AQaq"2�