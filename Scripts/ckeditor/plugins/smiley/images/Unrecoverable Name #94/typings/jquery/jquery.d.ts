r>
    <member name="P:System.Web.Http.Validation.ModelValidator.IsRequired">
      <summary>Gets a value that indicates whether a model property is required.</summary>
      <returns>true if the model property is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates a specified object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidator.ValidatorProviders">
      <summary>Gets or sets an enumeration of validator providers.</summary>
      <returns>An enumeration of validator providers.</returns>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidatorProvider">
      <summary>Provides a list of validators for a model.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets a list of validators associated with this <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" />.</summary>
      <returns>The list of validators.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.AssociatedValidatorProvider">
      <summary>Provides an abstract class for classes that implement a validation provider.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.AssociatedValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetTypeDescriptor(System.Type)">
      <summary>Gets a type descriptor for the specified type.</summary>
      <returns>A type descriptor for the specified type.</returns>
      <param name="type">The type of the validation provider.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets the validators for the model using the metadata and validator providers.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumeration of validator providers.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model using the metadata, the validator providers, and a list of attributes.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumeration of validator providers.</param>
      <param name="attributes">The list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidationFactory">
      <summary>Represents the method that creates a <see cref="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider" /> instance.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider">
      <summary>Represents an implementation of <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> which providers validators for attributes which derive from <see cref="T:System.ComponentModel.DataAnnotations.ValidationAttribute" />. It also provides a validator for types which implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. To support client side validation, you can either register adapters through the static methods on this class, or by having your validation attributes implement <see cref="T:System.Web.Http.Validation.IClientValidatable" />. The logic to support IClientValidatable is implemented in <see cref="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator" />. </summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model using the specified metadata, validator provider and attributes.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attributes">The attributes.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterAdapter(System.Type,System.Type)">
      <summary>Registers an adapter to provide client-side validation.</summary>
      <param name="attributeType">The type of the validation attribute.</param>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterAdapterFactory(System.Type,System.Web.Http.Validation.Providers.DataAnnotationsModelValidationFactory)">
      <summary>Registers an adapter factory for the validation provider.</summary>
      <param name="attributeType">The type of the attribute.</param>
      <param name="factory">The factory that will be used to create the <see cref="T:System.Web.Http.Validation.ModelValidator" /> object for the specified attribute.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterDefaultAdapter(System.Type)">
      <summary>Registers the default adapter.</summary>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterDefaultAdapterFactory(System.Web.Http.Validation.Providers.DataAnnotationsModelValidationFactory)">
      <summary>Registers the default adapter factory.</summary>
      <param name="factory">The factory that will be used to create the <see cref="T:System.Web.Http.Validation.ModelValidator" /> object for the default adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterDefaultValidatableObjectAdapter(System.Type)">
      <summary>Registers the default adapter type for objects which implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. The adapter type must derive from <see cref="T:System.Web.Http.Validation.ModelValidator" /> and it must contain a public constructor which takes two parameters of types <see cref="T:System.Web.Http.Metadata.ModelMetadata" /> and <see cref="T:System.Web.Http.Controllers.HttpActionContext" />. </summary>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterDefaultValidatableObjectAdapterFactory(System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory)">
      <summary>Registers the default adapter factory for objects which implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. </summary>
      <param name="factory">The factory.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterValidatableObjectAdapter(System.Type,System.Type)">
      <summary>Registers an adapter type for the given modelType, which must implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. The adapter type must derive from <see cref="T:System.Web.Http.Validation.ModelValidator" /> and it must contain a public constructor which takes two parameters of types <see cref="T:System.Web.Http.Metadata.ModelMetadata" /> and <see cref="T:System.Web.Http.Controllers.HttpActionContext" />. </summary>
      <param name="modelType">The model type.</param>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterValidatableObjectAdapterFactory(System.Type,System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory)">
      <summary>Registers an adapter factory for the given modelType, which must implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. </summary>
      <param name="modelType">The model type.</param>
      <param name="factory">The factory.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory">
      <summary>Provides a factory for validators that are based on <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider">
      <summary>Represents a validator provider for data member model.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumerator of validator providers.</param>
      <param name="attributes">A list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider">
      <summary>An implementation of <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> which provides validators that throw exceptions when the model is invalid.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets a list of validators associated with this <see cref="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider" />.</summary>
      <returns>The list of validators.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attributes">The list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider">
      <summary>Represents the provider for the required member model validator.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider.#ctor(System.Net.Http.Formatting.IRequiredMemberSelector)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider" /> class.</summary>
      <param name="requiredMemberSelector">The required member selector.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets the validator for the member model.</summary>
      <returns>The validator for the member model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator">
      <summary>Provides a model validator.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.ComponentModel.DataAnnotations.ValidationAttribute)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator" /> class.</summary>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attribute">The validation attribute for the model.</param>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.Attribute">
      <summary>Gets or sets the validation attribute for the model validator.</summary>
      <returns>The validation attribute for the model validator.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.IsRequired">
      <summary>Gets a value that indicates whether model validation is required.</summary>
      <returns>true if model validation is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates the model and returns the validation errors if any.</summary>
      <returns>A list of validation error messages for the model, or an empty list if no errors have occurred.</returns>
      <param name="metadata">The model metadata.</param>
      <param name="container">The container for the model.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.ErrorModelValidator">
      <summary>A <see cref="T:System.Web.Http.Validation.ModelValidator" /> to represent an error. This validator will always throw an exception regardless of the actual model value.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ErrorModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.ErrorModelValidator" /> class.</summary>
      <param name="validatorProviders">The list of  model validator providers.</param>
      <param name="errorMessage">The error message for the exception.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ErrorModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates a specified object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.RequiredMemberModelValidator">
      <summary>Represents the <see cref="T:System.Web.Http.Validation.ModelValidator" /> for required members. </summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.RequiredMemberModelValidator" /> class.</summary>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.IsRequired">
      <summary>Gets or sets a value that instructs the serialization engine that the member must be presents when validating.</summary>
      <returns>true if the member is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates the object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.ValidatableObjectAdapter">
      <summary>Provides an object adapter that can be validated.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ValidatableObjectAdapter.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.ValidatableObjectAdapter" /> class.</summary>
      <param name="validatorProviders">The validation provider.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ValidatableObjectAdapter.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates the specified object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.IEnumerableValueProvider">
      <summary>Represents the base class for value providers whose values come from a collection that implements the <see cref="T:System.Collections.IEnumerable" /> interface.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.IEnumerableValueProvider.GetKeysFromPrefix(System.String)">
      <summary>Retrieves the keys from the specified <paramref name="prefix" />.</summary>
      <returns>The keys from the specified <paramref name="prefix" />.</returns>
      <param name="prefix">The prefix.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.IValueProvider">
      <summary>Defines the methods that are required for a value provider in ASP.NET MVC.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.IValueProvider.ContainsPrefix(System.String)">
      <summary>Determines whether the collection contains the specified prefix.</summary>
      <returns>true if the collection contains the specified prefix; otherwise, false.</returns>
      <param name="prefix">The prefix to search for.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.IValueProvider.GetValue(System.String)">
      <summary>Retrieves a value object using the specified key.</summary>
      <returns>The value object for the specified key.</returns>
      <param name="key">The key of the value object to retrieve.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.ValueProviderAttribute">
      <summary> This attribute is used to specify a custom <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" />. </summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderAttribute.#ctor(System.Type)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.ValueProviderAttribute" />.</summary>
      <param name="valueProviderFactory">The type of the model binder.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderAttribute.#ctor(System.Type[])">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.ValueProviderAttribute" />.</summary>
      <param name="valueProviderFactories">An array of model binder types.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderAttribute.GetValueProviderFactories(System.Web.Http.HttpConfiguration)">
      <summary>Gets the value provider factories.</summary>
      <returns>A collection of value provider factories.</returns>
      <param name="configuration">A configuration object.</param>
    </member>
    <member name="P:System.Web.Http.ValueProviders.ValueProviderAttribute.ValueProviderFactoryTypes">
      <summary>Gets the types of object returned by the value provider factory.</summary>
      <returns>A collection of types.</returns>
    </member>
    <member name="T:System.Web.Http.ValueProviders.ValueProviderFactory">
      <summary>Represents a factory for creating value-provider objects.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderFactory.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Returns a value-provider object for the specified controller context.</summary>
      <returns>A value-provider object.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.ValueProviderResult">
      <summary>Represents the result of binding a value (such as from a form post or query string) to an action-method argument property, or to the argument itself.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderResult.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.ValueProviderResult" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderResult.#ctor(System.Object,System.String,System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.ValueProviderResult" /> class.</summary>
      <param name="rawValue">The raw value.</param>
      <param name="attemptedValue">The attempted value.</param>
      <param name="culture">The culture.</param>
    </member>
    <member name="P:System.Web.Http.ValueProviders.ValueProviderResult.AttemptedValue">
      <summary>Gets or sets the raw value that is converted to a string for display.</summary>
      <returns>The raw value that is converted to a string for display.</returns>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderResult.ConvertTo(System.Type)">
      <summary>Converts the value that is encapsulated by this result to the specified type.</summary>
      <returns>The converted value.</returns>
      <param name="type">The target type.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.ValueProviderResult.ConvertTo(System.Type,System.Globalization.CultureInfo)">
      <summary>Converts the value that is encapsulated by this result to the specified type by using the specified culture information.</summary>
      <returns>The converted value.</returns>
      <param name="type">The target type.</param>
      <param name="culture">The culture to use in the conversion.</param>
    </member>
    <member name="P:System.Web.Http.ValueProviders.ValueProviderResult.Culture">
      <summary>Gets or sets the culture.</summary>
      <returns>The culture.</returns>
    </member>
    <member name="P:System.Web.Http.ValueProviders.ValueProviderResult.RawValue">
      <summary>Gets or set the raw value that is supplied by the value provider.</summary>
      <returns>The raw value that is supplied by the value provider.</returns>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.CompositeValueProvider">
      <summary>Represents a 