<returns>True if model is valid, false otherwise.</returns>
      <param name="model">The model to be validated.</param>
      <param name="type">The <see cref="T:System.Type" /> to use for validation.</param>
      <param name="metadataProvider">The <see cref="T:System.Web.Http.Metadata.ModelMetadataProvider" /> used to provide the model metadata.</param>
      <param name="actionContext">The <see cref="T:System.Web.Http.Controllers.HttpActionContext" /> within which the model is being validated.</param>
      <param name="keyPrefix">The <see cref="T:System.String" /> to append to the key for any validation errors.</param>
    </member>
    <member name="T:System.Web.Http.Validation.IBodyModelValidator">
      <summary>Represents an interface for the validation of the models</summary>
    </member>
    <member name="M:System.Web.Http.Validation.IBodyModelValidator.Validate(System.Object,System.Type,System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.String)">
      <summary> Determines whether the model is valid and adds any validation errors to the actionContext's <see cref="T:System.Web.Http.ModelBinding.ModelStateDictionary" /></summary>
      <returns>trueif model is valid, false otherwise.</returns>
      <param name="model">The model to be validated.</param>
      <param name="type">The <see cref="T:System.Type" /> to use for validation.</param>
      <param name="metadataProvider">The <see cref="T:System.Web.Http.Metadata.ModelMetadataProvider" /> used to provide the model metadata.</param>
      <param name="actionContext">The <see cref="T:System.Web.Http.Controllers.HttpActionContext" /> within which the model is being validated.</param>
      <param name="keyPrefix">The <see cref="T:System.String" /> to append to the key for any validation errors.</param>
    </member>
    <member name="T:System.Web.Http.Validation.ModelStateFormatterLogger">
      <summary>This <see cref="T:System.Net.Http.Formatting.IFormatterLogger" /> logs formatter errors to the provided <see cref="T:System.Web.Http.ModelBinding.ModelStateDictionary" />.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelStateFormatterLogger.#ctor(System.Web.Http.ModelBinding.ModelStateDictionary,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelStateFormatterLogger" /> class.</summary>
      <param name="modelState">The model state.</param>
      <param name="prefix">The prefix.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelStateFormatterLogger.LogError(System.String,System.Exception)">
      <summary>Logs the specified model error.</summary>
      <param name="errorPath">The error path.</param>
      <param name="exception">The error message.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelStateFormatterLogger.LogError(System.String,System.String)">
      <summary>Logs the specified model error.</summary>
      <param name="errorPath">The error path.</param>
      <param name="errorMessage">The error message.</param>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidatedEventArgs">
      <summary>Provides data for the <see cref="E:System.Web.Http.Validation.ModelValidationNode.Validated" /> event.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatedEventArgs.#ctor(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.Validation.ModelValidationNode)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidatedEventArgs" /> class.</summary>
      <param name="actionContext">The action context.</param>
      <param name="parentNode">The parent node.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidatedEventArgs.ActionContext">
      <summary>Gets or sets the context for an action.</summary>
      <returns>The context for an action.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidatedEventArgs.ParentNode">
      <summary>Gets or sets the parent of this node.</summary>
      <returns>The parent of this node.</returns>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidatingEventArgs">
      <summary>Provides data for the <see cref="E:System.Web.Http.Validation.ModelValidationNode.Validating" /> event.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatingEventArgs.#ctor(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.Validation.ModelValidationNode)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidatingEventArgs" /> class.</summary>
      <param name="actionContext">The action context.</param>
      <param name="parentNode">The parent node.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidatingEventArgs.ActionContext">
      <summary>Gets or sets the context for an action.</summary>
      <returns>The context for an action.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidatingEventArgs.ParentNode">
      <summary>Gets or sets the parent of this node.</summary>
      <returns>The parent of this node.</returns>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidationNode">
      <summary>Provides a container for model validation information.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.#ctor(System.Web.Http.Metadata.ModelMetadata,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidationNode" /> class, using the model metadata and state key.</summary>
      <param name="modelMetadata">The model metadata.</param>
      <param name="modelStateKey">The model state key.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.#ctor(System.Web.Http.Metadata.ModelMetadata,System.String,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidationNode})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidationNode" /> class, using the model metadata, the model state key, and child model-validation nodes.</summary>
      <param name="modelMetadata">The model metadata.</param>
      <param name="modelStateKey">The model state key.</param>
      <param name="childNodes">The model child nodes.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.ChildNodes">
      <summary>Gets or sets the child nodes.</summary>
      <returns>The child nodes.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.CombineWith(System.Web.Http.Validation.ModelValidationNode)">
      <summary>Combines the current <see cref="T:System.Web.Http.Validation.ModelValidationNode" /> instance with a specified <see cref="T:System.Web.Http.Validation.ModelValidationNode" /> instance.</summary>
      <param name="otherNode">The model validation node to combine with the current instance.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.ModelMetadata">
      <summary>Gets or sets the model metadata.</summary>
      <returns>The model metadata.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.ModelStateKey">
      <summary>Gets or sets the model state key.</summary>
      <returns>The model state key.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.SuppressValidation">
      <summary>Gets or sets a value that indicates whether validation should be suppressed.</summary>
      <returns>true if validation should be suppressed; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.Validate(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Validates the model using the specified execution context.</summary>
      <param name="actionContext">The action context.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.Validate(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.Validation.ModelValidationNode)">
      <summary>Validates the model using the specified execution context and parent node.</summary>
      <param name="actionContext">The action context.</param>
      <param name="parentNode">The parent node.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.ValidateAllProperties">
      <summary>Gets or sets a value that indicates whether all properties of the model should be validated.</summary>
      <returns>true if all properties of the model should be validated, or false if validation should be skipped.</returns>
    </member>
    <member name="E:System.Web.Http.Validation.ModelValidationNode.Validated">
      <summary>Occurs when the model has been validated.</summary>
    </member>
    <member name="E:System.Web.Http.Validation.ModelValidationNode.Validating">
      <summary>Occurs when the model is being validated.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidationRequiredMemberSelector">
      <summary>Represents the selection of required members by checking for any required ModelValidators associated with the member.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidation