Infrastructure.SqlCeConnectionFactory"/> is used to create connections to Microsoft SQL
                Server Compact Editions.
                Other implementations for other database servers can be added as needed.
                Note that implementations should be thread safe or immutable since they may
                be accessed by multiple threads at the same time.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.IDbConnectionFactory.CreateConnection(System.String)">
            <summary>
                Creates a connection based on the given database name or connection string.
            </summary>
            <param name = "nameOrConnectionString">The database name or connection string.</param>
            <returns>An initialized DbConnection.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.Local