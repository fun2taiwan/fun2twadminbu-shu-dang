        </member>
        <member name="P:System.Data.Entity.Migrations.Model.IndexOperation.HasDefaultName">
            <summary>
                Gets a value indicating if a specific name has been supplied for this index.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.IndexOperation.Name">
            <summary>
                Gets or sets the name of this index.
                If no name is supplied, a default name will be calculated.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.CreateIndexOperation.#ctor(System.Object)">
            <summary>
                Initializes a new instance of the CreateIndexOperation class.
                The Table and Columns properties should also be populated.
            </summary>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 