ing whether the spatial type is to be type checked strictly.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.EdmPrimitiveTypeKind">
            <summary>
                Primitive Types as defined by the Entity Data Model (EDM).
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.Binary">
            <summary>
                Binary Type Kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.Boolean">
            <summary>
                Boolean Type Kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.Byte">
            <summary>
                Byte Type Kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.DateTime">
            <summary>
                DateTime Type Kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.Decimal">
            <summary>
                Decimal Type Kind
          