ame="M:Newtonsoft.Json.Serialization.JsonPropertyCollection.#ctor(System.Type)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Serialization.JsonPropertyCollection"/> class.
            </summary>
            <param name="type">The type.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonPropertyCollection.GetKeyForItem(Newtonsoft.Json.Serialization.JsonProperty)">
            <summary>
            When implemented in a derived class, extracts the key from the specified element.
            </summary>
            <param name="item">The element from which to extract the key.</param>
            <returns>The key for the specified element.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonPropertyCollection.AddProperty(Newtonsoft.Json.Serialization.JsonProperty)">
            <summary>
            Adds a <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> object.
            </summary>
            <param name="property">The property to add to the collection.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonPropertyCollection.GetClosestMatchProperty(System.String)">
            <summary>
            Gets the closest matching <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> object.
            First attempts to get an exact case match of propertyName and then
            a case insensitive match.
            </summary>
            <param name="propertyName">Name of the property.</param>
            <returns>A matching property if found.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonPropertyCollection.GetProperty(System.String,System.StringComparison)">
            <summary>
            Gets a property by property name.
            </summary>
            <param name="propertyName">The name of the property to get.</param>
            <param name="comparisonType">Type property name string comparison.</param>
            <returns>A matching property if found.</returns>
        </member>
        <member name="T:Newtonsoft.Json.Serialization.JsonStringContract">
            <summary>
            Contract details for a <see cref="T:System.Type"/> used by the <see cref="T:Newtonsoft.Json.JsonSerializer"/>.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonStringContract.#ctor(System.Type)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Serialization.JsonStringContract"/> class.
            </summary>
            <param name="underlyingType">The underlying type for the contract.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonTypeReflector.CreateJsonConverterInstance(System.Type,System.Object[])">
            <summary>
            Lookup and create an instance of the JsonConverter type described by the argument.
            </summary>
            <param name="converterType">The JsonConverter type to create.</param>
            <param name="converterArgs">Optional arguments to pass to an initializing constructor of the JsonConverter.
            If null, the default constructor is used.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.JsonTypeReflector.GetJsonConverterCreator(System.Type)">
            <summary>
            Create a factory function that can be used to create instances of a JsonConverter described by the 
            argument type.  The returned function can then be used to either invoke the converter's default ctor, or any 
            parameterized constructors by way of an object array.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Serialization.MemoryTraceWriter">
            <summary>
            Represents a trace writer that writes to memory. When the trace message limit is
            reached then old trace messages will be removed as new messages are added.
            </summary>
        </member>
      