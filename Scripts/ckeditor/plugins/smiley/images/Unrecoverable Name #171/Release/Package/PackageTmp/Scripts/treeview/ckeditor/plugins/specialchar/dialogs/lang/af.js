            Converts the <see cref="T:System.UInt32"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.UInt32"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Int64)">
            <summary>
            Converts the <see cref="T:System.Int64"/>  to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Int64"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.UInt64)">
            <summary>
            Converts the <see cref="T:System.UInt64"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.UInt64"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Single)">
            <summary>
            Converts the <see cref="T:System.Single"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Single"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Double)">
            <summary>
            Converts the <see cref="T:System.Double"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Double"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Byte)">
            <summary>
            Converts the <see cref="T:System.Byte"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Byte"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.SByte)">
            <summary>
            Converts the <see cref="T:System.SByte"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.SByte"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Decimal)">
            <summary>
            Converts the <see cref="T:System.Decimal"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.SByte"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Guid)">
            <summary>
            Converts the <see cref="T:System.Guid"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Guid"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.TimeSpan)">
            <summary>
            Converts the <see cref="T:System.TimeSpan"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.TimeSpan"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.Uri)">
            <summary>
            Converts the <see cref="T:System.Uri"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.Uri"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.String)">
            <summary>
            Converts the <see cref="T:System.String"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</