ummary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.System#ComponentModel#IListSource#GetList">
            <summary>
                Throws an exception indicating that binding directly to a store query is not supported.
                Instead populate a DbSet with data, for example by using the Load extension method, and
                then bind to local data.  For WPF bind to DbSet.Local.  For Windows Forms bind to
                DbSet.Local.ToBindingList().
            </summary>
            <returns>
                Never returns; always throws.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.System#Collections#IEnumerable#GetEnumerator">
            <summary>
                Gets the enumeration of this query causing it to be executed against the store.
            </summary>
            <returns>An enumerator for the query</returns>
        </member>
        <!-- Badly formed XML comment ignored for member "M:System.Data.Entity.Infrastructure.DbQuery.Include(System.String)" -->
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.AsNoTracking">
            <summary>
                Returns a new query where the entities returned will not be cached in the <see cref="T:System.Data.Entity.DbContext"/>.
            </summary>
            <returns> A new query with NoTracking applied.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.Cast``1">
            <summary>
                Returns the equivalent generic <see cref="T:System.Data.Entity.Infrastructure.DbQuery`1"/> object.
            </summary>
            <typeparam name="TElement">The type of element for which the query was created.</typeparam>
            <returns>The generic set object.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.ToString">
            <summary>
                Returns a <see cref="T:System.String"/> representation of the underlying query.
            </summary>
            <returns>
                The query string.
            </returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.System#ComponentModel#IListSource#ContainsListCollection">
            <summary>
                Returns <c>false</c>.
            </summary>
            <returns><c>false</c>.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.ElementType">
            <summary>
                The IQueryable element type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.System#Linq#IQueryable#Expression">
            <summary>
                The IQueryable LINQ Expression.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.System#Linq#IQueryable#Provider">
            <summary>
                The IQueryable provider.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.InternalQuery">
            <summary>
                Gets the underlying internal query object.
            </summary>
            <value>The internal query.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery.System#Data#Entity#Internal#Linq#IInternalQueryAdapter#InternalQuery">
            <summary>
                The internal query object that is backing this DbQuery
            </summary>
        </member>
        <member name="T:System.Data.Entity.Internal.Linq.IInternalSetAdapter">
            <summary>
                An internal interface implemented by <see cref="T:System.Data.Entity.DbSet`1"/> and <see cref="T:System.Data.Entity.DbSet"/> that allows access to
                the internal set without using reflection