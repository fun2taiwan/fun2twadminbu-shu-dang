, in which case the name must be found in the config file or an exception will be thrown.
            
                Note that the connection found in the app.config or web.config file can be a normal database connection
                string (not a special Entity Framework connection string) in which case the DbContext will use Code First.
                However, if the connection found in the config file is a special Entity Framework connection string, then the
                DbContext will use Database/Model First and the model specified in the connection string will be used.
            
                An existing or explicitly created DbConnection can also be used instead of the database/connection name.
            
                A <see cref="T:System.Data.Entity.DbModelBuilderVersionAttribute"/> can be applied to a class derived from DbContext to set the
                version of conventions used by the context when it creates a model. If no attribute is applied then the
                latest version of conventions will be used.
            </remarks>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.IObjectContextAdapter">
            <summary>
             