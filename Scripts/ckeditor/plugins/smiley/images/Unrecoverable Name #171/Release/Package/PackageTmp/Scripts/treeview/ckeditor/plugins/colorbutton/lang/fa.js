param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.AlterColumnOperation.Table">
            <summary>
                Gets the name of the table that the column belongs to.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.AlterColumnOperation.Column">
            <summary>
                Gets the new definition for the column.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.AlterColumnOperation.Inverse">
            <summary>
                Gets an operation that represents reverting the alteration.
                The inverse cannot be automatically calculated, 
                if it was not supplied to the constructor this property will return null.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.AlterColumnOperation.IsDestructiveChange">
            <inheritdoc />
        </member>
        <member name="T:System.Data.Entity.Migrations.Model.ColumnModel">
            <summary>
                Represents information about a column.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.ColumnModel.#ctor(S