set"/> parameter is a custom or fake IDbSet implementation, this method will
                attempt to locate and invoke a public, instance method with the same signature as this extension method.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.Migrations.IDbSetExtensions.AddOrUpdate``1(System.Data.Entity.IDbSet{``0},System.Linq.Expressions.Expression{System.Func{``0,System.Object}},``0[])">
            <summary>
                Adds or updates entities by a custom identification expression when SaveChanges is called.
                Equivalent to an "upsert" operation from database terminology.
                This method can useful when seeding data using Migrations.
            </summary>
            <param name = "identifierExpression">
                An expression specifying the properties that should be used when determining
      