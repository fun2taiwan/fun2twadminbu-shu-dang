text being created.</param>
        </member>
        <member name="M:System.Data.Entity.DbContext.CallOnModelCreating(System.Data.Entity.DbModelBuilder)">
            <summary>
                Internal method used to make the call to the real OnModelCreating method.
            </summary>
            <param name = "modelBuilder">The model builder.</param>
        </member>
        <member name="M:System.Data.Entity.DbContext.Set``1">
            <summary>
                Returns a DbSet instance for access to entities of the given type in the context,
                the ObjectStateManager, and the underlying store.
            </summary>
            <remarks>
                See the DbSet class for more details.
            </remarks>
            <typeparam name = "TEntity">The type entity for which a set should be returned.</typeparam>
            <returns>A set for the given entity type.</returns>
        </member>
        <member name="M:System