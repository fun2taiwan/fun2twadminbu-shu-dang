vider"/> used by the serializer to get and set values from a member.
            </summary>
            <param name="member">The member.</param>
            <returns>The <see cref="T:Newtonsoft.Json.Serialization.IValueProvider"/> used by the serializer to get and set values from a member.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultContractResolver.CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> for the given <see cref="T:System.Reflection.MemberInfo"/>.
            </summary>
            <param name="memberSerialization">The member's parent <see cref="T:Newtonsoft.Json.MemberSerialization"/>.</param>
            <param name="member">The member to create a <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> for.</param>
            <returns>A created <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> for the given <see cref="T:System.Reflection.MemberInfo"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultContractResolver.ResolvePropertyName(System.String)">
            <summary>
            Resolves the name of the property.
            </summary>
            <param name="propertyName">Name of the property.</param>
            <returns>Resolved name of the property.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultContractResolver.ResolveDictionaryKey(System.String)">
            <summary>
            Resolves the key of the dictionary. By default <see cref="M:Newtonsoft.Json.Serialization.DefaultContractResolver.ResolvePropertyName(System.String)"/> is used to resolve dictionary keys.
            </summary>
            <param name="dictionaryKey">Key of the dictionary.</param>
            <returns>Resolved key of the dictionary.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultContractResolver.GetResolvedPropertyName(System.String)">
            <summary>
            Gets the resolved name of the property.
            </summary>
            <param name="propertyName">Name of the property.</param>
            <returns>Name of the property.</returns>
        </member>
        <member name="T:Newtonsoft.Json.Serialization.DefaultSerializationBinder">
            <summary>
            The default serialization binder used when resolving and loading classes from type names.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultSerializationBinder.BindToType(System.String,System.String)">
            <summary>
            When overridden in a derived class, controls the binding of a serialized object to a type.
            </summary>
            <param name="assemblyName">Specifies the <see cref="T:System.Reflection.Assembly"/> name of the serialized object.</param>
            <param name="typeName">Specifies the <see cref="T:System.Type"/> name of the serialized object.</param>
            <returns>
            The type of the object the formatter creates a new instance of.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.DefaultSerializationBinder.BindToName(System.Type,System.String@,System.String@)">
            <summary>
            When overridden in a derived class, controls the binding of a serialized object to a type.
            </summary>
            <param name="serializedType">The type of the object the formatter creates a new instance of.</param>
            <param name="assemblyName">Specifies the <see cref="T:System.Reflection.Assembly"/> name of the serialized object. </param>
            <param name="typeName">Specifies the <see cref="T:System.Type"/> name of the serialized object. </param>
        </member>
        <member name="T:Newtonsoft.Json.Serialization.DiagnosticsTraceWriter">
            <summary>
            Represents a trace writer that writes to the application's <see cref="T:System.Diagnostics.TraceListener"/> instances.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.DiagnosticsTraceWriter.LevelFilter">
            <summary>
            Gets the <see cref="T:System.Diagnostics.TraceLevel"/> that will be used to filter the trace messages passed to the writer.
            For example a filter level of <code>Info</code> will exclude <code>Verbose</code> messages and include <code>Info</code>,
            <code>Warning</code> and <code>Error</code> messages.
            </summary>
            <value>
            The 