ystem.Char[])">
            <summary>
                Writes a character array to the text stream.
            </summary>
            <param name = "buffer">The character array to write. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.Write(System.Char[],System.Int32,System.Int32)">
            <summary>
                Writes a subarray of characters to the text stream.
            </summary>
            <param name = "buffer">The character array to write data from. </param>
            <param name = "index">Starting index in the buffer. </param>
            <param name = "count">The number of characters to write. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.Write(System.Double)">
            <summary>
                Writes the text represent