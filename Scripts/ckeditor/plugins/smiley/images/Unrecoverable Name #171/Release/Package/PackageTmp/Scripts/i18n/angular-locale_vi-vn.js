eturn
                types are:
                Reference navigation property: <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry"/>.
                Collection navigation property: <see cref="T:System.Data.Entity.Infrastructure.DbCollectionEntry"/>.
                Primitive/scalar property: <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry"/>.
                Complex property: <see cref="T:System.Data.Entity.Infrastructure.DbComplexPropertyEntry"/>.
            </summary>
            <param name="propertyName">The name of the member.</param>
            <returns>An object representing the member.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Member``1(System.String)">
            <summary>
                Gets an object that represents a member of the entity.  The runtime type of the returned object will
                vary depending on what kind of member is asked for.  The currently supported member types and their return
                types are:
                Reference navigation property: <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2"/>.
                Collection navigation property: <see cref="T:System.Data.Entity.Infrastructure.DbCollectionEntry`2"/>.
                Primitive/scalar property: <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry`2"/>.
                Complex property: <see cref="T:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2"/>.
            </summary>
            <typeparam name="TMember">The type of the member.</typeparam>
            <param name="propertyName">The name of the member.</param>
            <returns>An object representing the member.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.op_Implicit(System.Data.Entity.Infrastructure.DbEntityEntry{`0})~System.Data.Entity.Infrastructure.DbEntityEntry">
            <summary>
                Returns a new instance of the non-generic <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> class for 
                the tracked entity represented by this object.
            </s