ata.Entity.Migrations.Model.CreateIndexOperation,System.Object)">
            <summary>
                Initializes a new instance of the DropIndexOperation class.
            </summary>
            <param name = "inverse">The operation that represents reverting dropping the index.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.DropIndexOperation.Inverse">
            <summary>
                Gets an operation that represents reverting dropping the index.
                The inverse cannot be automatically calculated, 
                if it was not supplied to the constructor this property