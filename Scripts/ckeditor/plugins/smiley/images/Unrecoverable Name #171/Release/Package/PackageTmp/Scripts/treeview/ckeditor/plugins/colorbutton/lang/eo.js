f the table the column should be dropped from.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.DropColumnOperation.Name">
            <summary>
                Gets the name of the column to be dropped.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.DropColumnOperation.Inverse">
            <summary>
                Gets an operation that represents reverting dropping the column.
                The inverse cannot be automatically calculated, 
                if it was not supplied to the constructor this property will return null.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.DropColumnOperation.IsDestructiveChange">
            <inheritdoc />
        </member>
        <member name="T:System.Data.Entity.Migrations.Model.DropForeignKeyOperation">
            <summary>
                Re