IEnumerable{System.Data.Entity.Migrations.Model.MigrationOperation},System.String,System.String,System.String,System.String)">
            <inheritdoc />
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Collections.Generic.IEnumerable{System.Data.Entity.Migrations.Model.MigrationOperation},System.String,System.String)">
            <summary>
                Generates the primary code file that the user can view and edit.
            </summary>
            <param name = "operations">Operations to be performed by the migration.</param>
            <param name = "namespace">Namespace that code should be generated in.</param>
            <param name = "className">Name of the class that should be generated.</param>
            <returns>The generated code.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Stri