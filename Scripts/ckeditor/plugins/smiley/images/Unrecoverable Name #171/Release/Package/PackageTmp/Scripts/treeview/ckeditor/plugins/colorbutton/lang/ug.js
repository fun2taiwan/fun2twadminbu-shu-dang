ta.Entity.Internal.DatabaseCreator.CreateDatabase(System.Data.Entity.Internal.InternalContext,System.Func{System.Data.Entity.Migrations.DbMigrationsConfiguration,System.Data.Entity.DbContext,System.Data.Entity.Migrations.DbMigrator},System.Data.Objects.ObjectContext)">
            <summary>
                Creates a database using the core provider (i.e. ObjectContext.CreateDatabase) or
                by using Code First Migrations <see cref="T:System.Data.Entity.Migrations.DbMigrator"/> to create an empty database
                and the perform an automatic migration to the current model.
                Migrations is used if Code First is being used and the EF provider is for SQL Server
                or SQL Compact. The core is used for non-Code First models and for other providers even
                when using Code First.
            </summary>
        </member>
        <member name="T:System.Data.Entity.DbContext">
            <summary>
                A DbContext instance represents a combination of the Unit Of Work and Repository patterns such that
                it can be used to query from a database and group together changes that will then be written
                back to the store as a unit.
                DbContext is conceptually similar to ObjectContext.
            </summary>
            <remarks>
