 instances are considered equal if they are both entries for
                the same entity on the same <see cref="T:System.Data.Entity.DbContext"/>.
            </summary>
            <param name="other">The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> to compare with this instance.</param>
            <returns>
                <c>true</c> if the specified <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> is equal to this instance; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.GetHashCode">
            <summary>
                Returns a hash code for this instance.
            </summary>
            <returns>
                A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            </returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbEntityEntry`1.Entity">
            <summary>
                Gets the entity.
            </summary>
            <value>The entity.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbEntityEntry`1.State">
            <summary>
                Gets or sets the state of the entity.
            </summary>
            <value>The state.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbEntityEntry`1.CurrentValues">
            <summary>
                Gets the current property values for the tracked entity represented by this object.
            </summary>
            <value>The current values.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbEntityEntry`1.OriginalValues">
            <summary>
                Gets the original property values for the tracked entity represented by this object.
                The original values are usually the entity's property values as they were when last queried from
                the database.
            </summary>
            <value>The original values.</value>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbModel">
            <summary>
                Represents an Entity Data Model (EDM) created by the <see cref="T:System.Data.Entity.DbModelBuilder"/>.
                The Compile method can be used to go from this EDM representation to a <see cref="T:System.Data.Entity.Infrastructure.DbCompiledModel"/>
                which is a compiled snapshot of the model suitable for caching and creation of
                <see cref="T:System.Data.Entity.DbContext"/> or <see cref="T:System.Data.Objects.ObjectContext"/> instances.
            </summary>
    