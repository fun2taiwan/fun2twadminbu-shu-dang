,System.Type,System.Type,System.String,`0)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Metadata.Providers.CachedModelMetadata`1" /> class.</summary>
      <param name="provider">The provider.</param>
      <param name="containerType">The type of container.</param>
      <param name="modelType">The type of the model.</param>
      <param name="propertyName">The name of the property.</param>
      <param name="prototypeCache">The prototype cache.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.ComputeConvertEmptyStringToNull">
      <summary>Indicates whether empty strings that are posted back in forms should be computed and converted to null.</summary>
      <returns>true if empty strings that are posted back in forms should be computed and converted to null; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.ComputeDescription">
      <summary>Indicates the computation value.</summary>
      <returns>The computation value.</returns>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.ComputeIsComplexType">
      <summary>Gets a value that indicates whether the model is a complex type.</summary>
      <returns>A value that indicates whether the model is considered a complex type by the Web API framework.</returns>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.ComputeIsReadOnly">
      <summary>Gets a value that indicates whether the model to be computed is read-only.</summary>
      <returns>true if the model to be computed is read-only; otherwise, false.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.ConvertEmptyStringToNull">
      <summary>Gets or sets a value that indicates whether empty strings that are posted back in forms should be converted to null.</summary>
      <returns>true if empty strings that are posted back in forms should be converted to null; otherwise, false. The default value is true.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.Description">
      <summary>Gets or sets the description of the model.</summary>
      <returns>The description of the model.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.IsComplexType">
      <summary>Gets a value that indicates whether the model is a complex type.</summary>
      <returns>A value that indicates whether the model is considered a complex type by the Web API framework.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.IsReadOnly">
      <summary>Gets or sets a value that indicates whether the model is read-only.</summary>
      <returns>true if the model is read-only; otherwise, false.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedModelMetadata`1.PrototypeCache">
      <summary>Gets or sets a value that indicates whether the prototype cache is updating.</summary>
      <returns>true if the prototype cache is updating; otherwise, false.</returns>
    </member>
    <member name="T:System.Web.Http.Metadata.Providers.DataAnnotationsModelMetadataProvider">
      <summary>Implements the default model metadata provider.</summary>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.DataAnnotationsModelMetadataProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Metadata.Providers.DataAnnotationsModelMetadataProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.DataAnnotationsModelMetadataProvider.CreateMetadataFromPrototype(System.Web.Http.Metadata.Providers.CachedDataAnnotationsModelMetadata,System.Func{System.Object})">
      <summary>Creates the metadata from prototype for the specified property.</summary>
      <returns>The metadata for the property.</returns>
      <param name="prototype">The prototype.</param>
      <param name="modelAccessor">The model accessor.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.DataAnnotationsModelMetadataProvider.CreateMetadataPrototype(System.Collections.Generic.IEnumerable{System.Attribute},System.Type,System.Type,System.String)">
    