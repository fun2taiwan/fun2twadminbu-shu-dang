 that will
            use a specific connection string from the configuration file to connect to
            the database to perform the migration.
            </summary>
            <param name="connectionStringName">The name of the connection string to use for migration.</param>
        </member>
        <member name="M:System.Data.Entity.MigrateDatabaseToLatestVersion`2.InitializeDatabase(`0)">
            <inheritdoc/>
        </member>
        <member name="T:System.Data.Entity.Migrations.Builders.ColumnBuilder">
            <summary>
                Helper class that is used to configure a column.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Builders.ColumnBuilder.Binary(System.Nullable{System.Boolean},System.Nullable{System.Int32},System.Nullable{System.Boolean},System.Nullable{System.Boolean},System.Byte[],System.String,System.Boolean,System.String,System.String)">
            <summary>
                Creates a new column definition to store Binary data.
            </summary>
            <param name = "nullable">Value indicating whether or not the column allows null values.</param>
