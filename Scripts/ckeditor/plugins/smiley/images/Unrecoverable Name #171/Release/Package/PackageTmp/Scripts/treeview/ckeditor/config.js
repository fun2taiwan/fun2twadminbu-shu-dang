ries``1">
            <summary>
                Gets <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> objects for all the entities of the given type
                tracked by this context.
            </summary>
            <typeparam name="TEntity">The type of the entity.</typeparam>
            <returns>The entries.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbChangeTracker.DetectChanges">
            <summary>
                Detects changes made to the properties and relationships of POCO entities.  Note that some types of
                entity (such as change tracking proxies and entities that derive from <see cref="T:System.Data.Objects.DataClasses.EntityObject"/>)
                report changes automatically and a call to DetectChanges is not normally needed for these types of entities.
                Also note that normally DetectChanges is called automatically by many of the methods of <see cref="T:System.Data.Entity.DbContext"/>
                and its related classes such that it is rare that this method will need to be called explicitly.
                However, it may be desirable, usually for performance reasons, to turn off this automatic calling of
                DetectChanges using the AutoDetectChangesEnabled flag from <see cref="P:System.Data.Entity.DbContext.Configuration"/>.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbCollectionEntry">
            <summary>
                A non-generic version of the <see cref="T:System.Data.Entity.Infrastructure.DbCollectionEntry`2"/> class.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbMemberEntry">
            <summary>
                This is an abstract base class use to represent a scalar or complex property,