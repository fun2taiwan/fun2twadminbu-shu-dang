es an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="mediaType">The media type header value.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0,System.String)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="mediaType">The media type.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0,System.Web.Http.HttpConfiguration)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="configuration">The HTTP configuration which contains the dependency resolver used to resolve services.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.DisposeRequestResources(System.Net.Http.HttpRequestMessage)">
      <summary>Disposes of all tracked resources associated with the <paramref name="request" /> which were added via the <see cref="M:System.Net.Http.HttpRequestMessageExtensions.RegisterForDispose(System.Net.Http.HttpRequestMessage,System.IDisposable)" /> method.</summary>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetClientCertificate(System.Net.Http.HttpRequestMessage)">
      <summary>Gets the current X.509 certificate from the given HTTP request.</summary>
      <returns>The current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" />, or null if a certificate is not available.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetConfiguration(System.Net.Http.HttpRequestMessage)">
      <summary>Retrieves the <see cref="T:System.Web.Http.HttpConfiguration" /> for the given request.</summary>
      <returns>The <see cref="T:System.Web.Http.HttpConfiguration" /> for the given request.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetCorrelationId(System.Net.Http.HttpRequestMessage)">
      <summary>Retrieves the <see cref="T:System.Guid" /> which has been assigned as the correlation ID associated with the given <paramref name="request" />. The value will be created and set the first time this method is called.</summary>
      <returns>The <see cref="T:System.Guid" /> object that represents the correlation ID associated with the request.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetDependencyScope(System.Net.Http.HttpRequestMessage)">
      <summary>Retrieves the <see cref="T:System.Web.Http.Dependencies.IDependencyScope" /> for the given request or null if not available.</summary>
      <returns>The <see cref="T:System.Web.Http.Dependencies.IDependencyScope" /> for the given request or null if not available.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetQueryNameValuePairs(System.Net.Http.HttpRequestMessage)">
      <summary>Gets the parsed query string as a collection of key-value pairs.</summary>
      <returns>The query string as a collection of key-value pairs.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetRouteData(System.Net.Http.HttpRequestMessage)">
      <summary>Retrieves the <see cref="T:System.Web.Http.Routing.IHttpRouteData" /> for the given request or null if not available.</summary>
      <returns>The <see cref="T:System.Web.Http.Routing.IHttpRouteData" /> for the given request or null if not available.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetSynchronizationContext(System.Net.Http.HttpRequestMessage)">
      <summary>Retrieves the <see cref="T:System.Threading.SynchronizationContext" /> for the given request or null if not available.</summary>
      <returns>The <see cref="T:System.Threading.SynchronizationContext" /> for the given request or null if not available.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.GetUrlHelper(System.Net.Http.HttpRequestMessage)">
      <summary>Gets a <see cref="T:System.Web.Http.Routing.UrlHelper" /> instance for an HTTP request.</summary>
      <returns>A <see cref="T:System.Web.Http.Routing.UrlHelper" /> instance that is initialized for the specified HTTP request.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.RegisterForDispose(System.Net.Http.HttpRequestMessage,System.IDisposable)">
      <summary>Adds the given <paramref name="resource" /> to a list of resources that will be disposed by a host once the <paramref name="request" /> is disposed.</summary>
      <param name="request">The HTTP request controlling the lifecycle of <paramref name="resource" />.</param>
      <param name="resource">The resource to dispose when <paramref name="request" /> is being disposed.</param>
    </member>
    <member name="T:System.Net.Http.HttpResponseMessageExtensions">
      <summary>Represents the message extensions for the HTTP response from an ASP.NET operation.</summary>
    </member>
    <member name="M:System.Net.Http.HttpResponseMessageExtensions.TryGetContentValue``1(System.Net.Http.HttpResponseMessage,``0@)">
      <summary>Attempts to retrieve the value of the content for the <see cref="T:System.Net.Http.HttpResponseMessageExtensions" />.</summary>
      <returns>The result of the retrieval of value of the content.</returns>
      <param name="response">The response of the operation.</param>
      <param name="value">The value of the content.</param>
      <typeparam name="T">The type of the value to retrieve.</typeparam>
    </member>
    <member name="T:System.Net.Http.Formatting.MediaTypeFormatterExtensions">
      <summary>Represents extensions for adding <see cref="T:System.Net.Http.Formatting.MediaTypeMapping" /> items to a <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" />. </summary>
    </member>
    <member name="M:System.Net.Http.Formatting.MediaTypeFormatterExtensions.AddUriPathExtensionMapping(System.Net.Http.Formatting.MediaTypeFormatter,System.String,System.Net.Http.Headers.MediaTypeHeaderValue)">
      <summary> Updates the given formatter's set of <see cref="T:System.Net.Http.Formatting.MediaTypeMapping" /> elements so that it associates the mediaType with <see cref="T:System.Uri" />s ending with the given uriPathExtension. </summary>
      <param name="formatter">The <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> to receive the new <see cref="T:System.Net.Http.Formatting.UriPathExtensionMapping" /> item.</param>
      <param name="uriPathExtension">The string of the <see cref="T:System.Uri" /> path extension.</param>
      <param name="mediaType">The <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> to associate with <see cref="T:System.Uri" />s ending with uriPathExtension.</param>
    </member>
    <member name="M:System.Net.Http.Formatting.MediaTypeFormatterExtensions.AddUriPathExtensionMapping(System.Net.Http.Formatting.MediaTypeFormatter,System.String,System.String)">
      <summary> Updates the given formatter's set of <see cref="T:System.Net.Http.Formatting.MediaTypeMapping" /> elements so that it associates the mediaType with <see cref="T:System.Uri" />s ending with the given uriPathExtension. </summary>
      <param name="formatter">The <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> to receive the new <see cref="T:System.Net.Http.Formatting.UriPathExtensionMapping" /> item.</param>
      <param name="uriPathExtension">The string of the <see cref="T:System.Uri" /> path extension.</param>
      <param name="mediaType">The string media type to associate with <see cref="T:System.Uri" />s ending with uriPathExtension.</param>
    </member>
    <member name="T:System.Net.Http.Formatting.UriPathExtensionMapping">
      <summary>Provides <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" />s from path extensions appearing in a <see cref="T:System.Uri" />. </summary>
    </member>
    <member name="M:System.Net.Http.Formatting.UriPathExtensionMapping.#ctor(System.String,System.Net.Http.Headers.MediaTypeHeaderValue)">
      <summary> Initializes a new instance of the <see cref="T:System.Net.Http.Formatting.UriPathExtensionMapping" /> class. </summary>
      <param name="uriPathExtension">The extension corresponding to mediaType. This value should not include a dot or wildcards.</param>
      <param name="mediaType">The <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> that will be returned if uriPathExtension is matched.</param>
    </member>
    <member name="M:System.Net.Http.Formatting.UriPathExtensionMapping.#ctor(System.String,System.String)">
      <summary> Initializes a new instance of the <see cref="T:System.Net.Http.Formatting.UriPathExtensionMapping" /> class. </summary>
      <param name="uriPathExtension">The extension corresponding to mediaType. This value should not include a dot or wildcards.</param>
      <param name="mediaType">The media type that will be returned if uriPathExtension is matched.</param>
    </member>
    <member name="M:System.Net.Http.Formatting.UriPathE