     <remarks>
                It is necessary to provide the provider invariant name of the SQL Server Compact
                Edition to use when creating an instance of this class.  This is because different
                versions of SQL Server Compact Editions use different invariant names.
                An instance of this class can be set on the <see cref="T:System.Data.Entity.Database"/> class to
                cause all DbContexts created with no connection information or just a database
                name or connection string to use SQL Server Compact Edition by default.
                This class is immutable since multiple threads may access instances simultaneously
                when creating connections.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlCeConnectionFactory.#ctor(System.String)">
            <summary>
                Creates a new connection factory with empty (default) DatabaseDirectory and BaseConnectionString
                properties.
            </summary>
            <param name = "providerInvariantName">The provider invariant name that specifies the version of SQL Server Compact Edition that should be used.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlCeConnectionFactory.#ctor(System.String,System.String,System.String)">
            <summary>
                Creates a new connection factory with the given DatabaseDirectory and BaseConnectionString properties.
            </summary>
            <param name = "providerInvariantName">
                The provider invariant name that specifies the version of SQL Server Compact Edition that should be used.
            </param>
            <param name = "databaseDirectory">
                The path to prepend to the database name that will form the file name used by SQL Server Compact Edition
                when it creates or reads the database file. An empty string means that SQL Server Compact Edition will use
                its default for the database file location.
            </param>
            <param name = "baseConnectionString">
                The connection string to use for options to the database other than the 'Data Source'. The Data Source will
                be prepended to this string based on the database name when CreateConnection is called.
            </param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlCeConnectionFactory.CreateConnection(System.String)">
            <summary>
                Creates a connection for SQL Server Compact Edition based on the given database name or connection string.
                If the given string contains an '=' character then it is treated as a full connection string,
                otherwise it is treated as a database name only.
            </summary>
            <param name = "nameOrConnectionString">The database name or connection string.</param>
            <return