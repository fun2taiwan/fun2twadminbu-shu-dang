ary>
    </member>
    <member name="F:System.Web.Http.Routing.HttpRouteDirection.UriGeneration">
      <summary>The UriGeneration direction.</summary>
    </member>
    <member name="T:System.Web.Http.Routing.HttpRouteValueDictionary">
      <summary>Represents a route class for self-host of specified key/value pairs.</summary>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRouteValueDictionary.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRouteValueDictionary" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRouteValueDictionary.#ctor(System.Collections.Generic.IDictionary{System.String,System.Object})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRouteValueDictionary" /> class.</summary>
      <param name="dictionary">The dictionary.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRouteValueDictionary.#ctor(System.Object)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRouteValueDictionary" /> class.</summary>
      <param name="values">The key value.</param>
    </member>
    <member name="T:System.Web.Http.Routing.HttpVirtualPathData">
      <summary>Presents the data regarding the HTTP virtual path.</summary>
    </member>
    <member name="M:System.Web.Http.Routing.HttpVirtualPathData.#ctor(System.Web.Http.Routing.IHttpRoute,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpVirtualPathData" /> class.</summary>
      <param name="route">The route of the virtual path.</param>
      <param name="virtualPath">The URL that was created from the route definition.</param>
    </member>
    <member name="P:System.Web.Http.Routing.HttpVirtualPathData.Route">
      <summary>Gets or sets the route of the virtual path..</summary>
      <returns>The route of the virtual path.</returns>
    </member>
    <member name="P:System.Web.Http.Routing.HttpVirtualPathData.VirtualPath">
      <summary>Gets or sets the URL that was created from the route definition.</summary>
      <returns>The URL that was created from the route definition.</returns>
    </member>
    <member name="T:System.Web.Http.Routing.IHttpRoute">
      <summary>
        <see cref="T:System.Web.Http.Routing.IHttpRoute" /> defines the interface for a route expressing how to map an incoming <see cref="T:System.Net.Http.HttpRequestMessage" /> to a particular controller and action. </summary>
    </member>
    <member name="P:System.Web.Http.Routing.IHttpRoute.Constraints">
      <summary> Gets the constraints for the route parameters. </summary>
      <returns>The constraints for the route parameters.</returns>
    </member>
    <member name="P:System.Web.Http.Routing.IHttpRoute.DataTokens">
      <summary> Gets any additional data tokens not used directly to determine whether a route matches an incoming <see cref="T:System.Net.Http.HttpRequestMessage" />. </summary>
      <returns>The additional data tokens.</returns>
    </member>
    <member name="P:System.Web.Http.Routing.IHttpRoute.Defaults">
      <summary> Gets the default values for route parameters if not provided by the incoming <see cref="T:System.Net.Http.HttpRequestMessage" />. </summary>
      <returns>The default values for route parameters.</returns>
    </member>
    <member name="M:System.Web.Http.Routing.IHttpRoute.GetRouteData(System.String,System.Net.Http.HttpRequestMessage)">
      <summary> Determine whether this route is a match for the incoming request by looking up the &lt;see cref="!:IRouteData" /&gt; for the route. </summary>
      <returns>The &lt;see cref="!:RouteData" /&gt; for a route if matches; otherwise null.</returns>
      <param name="virtualPathRoot">The virtual path root.</param>
      <param name="request">The request.</param>
    </member>
    <member name="M:System.Web.Http.Routing.IHttpRoute.GetVirtualPath(System.Net.Http.HttpRequestMessage,System.Collections.Generic.IDictionary{System.String,System.Object})">
      <summary>Gets a virtual path data based on the route and the values provided.</summary>
      <returns>The virtual path data.</returns>
      <param name="request">The request message.</param>
      <param name="values">The values.</param>
    </member>
    <member name="P:System.Web.Http.Routing.IHttpRoute.Handler">
      <summary>Gets the message handler that will be the recipient of the reques