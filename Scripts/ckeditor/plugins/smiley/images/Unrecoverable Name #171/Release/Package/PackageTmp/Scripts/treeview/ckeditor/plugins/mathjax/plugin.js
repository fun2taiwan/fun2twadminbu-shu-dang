  </summary>
            <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.SetWriteState(Newtonsoft.Json.JsonToken,System.Object)">
            <summary>
            Sets the state of the JsonWriter,
            </summary>
            <param name="token">The JsonToken being written.</param>
            <param name="value">The value being written.</param>
        </member>
        <member name="T:Newtonsoft.Json.JsonWriterException">
            <summary>
            The exception thrown when an error occurs while reading JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriterException.Path">
            <summary>
            Gets the path to the JSON where the error occurred.
            </summary>
            <value>The path to the JSON where the error occurred.</value>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriterException.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonWriterException"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriterException.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonWriterException"/> class
            with a specified error message.
            </summary>
            <param name="message">The error message that explains the reason for the exception.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriterException.#ctor(System.String,System.Exception)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonWriterException"/> class
            with a specified error message and a reference to the inner exception that is the cause of this exception.
            </summary>
            <param name="message">The error message that explains the reason for the exception.</param>
            <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriterException.#ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonWriterException"/> class.
            </summary>
            <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
            <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
            <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
            <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
        </member>
        <member name="T:Newtonsoft.Json.Linq.CommentHandling">
            <summary>
            Specifies how JSON comments are handled when loading JSON.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.CommentHandling.Ignore">
            <summary>
            Ignore comments.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.CommentHandling.Load">
            <summary>
            Load comments as a <see cref="T:Newtonsoft.Json.Linq.JValue"/> with type <see cref="F:Newtonsoft.Json.Linq.JTokenType.Comment"/>.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Linq.LineInfoHandling">
            <summary>
            Specifies how line information is handled when loading JSON.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.LineInfoHandling.Ignore">
            <summary>
            Ignore line information.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.LineInfoHandling.Load">
            <summary>
            Load line information.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Linq.Extensions">
            <summary>
            Contains the LINQ to JSON extension methods.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Linq.Extensions.Ancestors``1(System.Collections.Generic.IEnumerable{``0})">
            <summary>
            Returns a collection of tokens that contains the ancestors of every token in the source collection.
            </summary>
            <typeparam name="T">The type of the objects in source, constrained to <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</typeparam>
            <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1"/> of <see cref="T:Newtonsoft.Json.Linq.JToken"/> that contains the source collection.</param>
            <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1"/> of <see cref="T:Newtonsoft.Json.Linq.JToken"/> that contains the ancestors of every token in the source collection.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.Extensions.AncestorsAndSelf``1(System.Collections.Generic.IEnumerable{``0})">
            <summary>
            Returns a collection of tokens that contains every token in the source collection, and the ancestors of every token in the source collecti