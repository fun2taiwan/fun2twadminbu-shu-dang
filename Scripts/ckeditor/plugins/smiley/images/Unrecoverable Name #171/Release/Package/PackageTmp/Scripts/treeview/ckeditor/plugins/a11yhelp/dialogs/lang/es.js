      <returns>An enumerator that can be used to iterate through the collection.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelStateDictionary.IsReadOnly">
      <summary>Gets a value that indicates whether the collection is read-only.</summary>
      <returns>true if the collection is read-only; otherwise, false.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelStateDictionary.IsValid">
      <summary>Gets a value that indicates whether this instance of the model-state dictionary is valid.</summary>
      <returns>true if this instance is valid; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.IsValidField(System.String)">
      <summary>Determines whether there are any <see cref="T:System.Web.Http.ModelBinding.ModelError" /> objects that are associated with or prefixed with the specified key.</summary>
      <returns>true if the model-state dictionary contains a value that is associated with the specified key; otherwise, false.</returns>
      <param name="key">The key.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelStateDictionary.Item(System.String)">
      <summary>Gets or sets the value that is associated with the specified key.</summary>
      <returns>The model state item.</returns>
      <param name="key">The key.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelStateDictionary.Keys">
      <summary>Gets a collection that contains the keys in the dictionary.</summary>
      <returns>A collection that contains the keys of the model-state dictionary.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.Merge(System.Web.Http.ModelBinding.ModelStateDictionary)">
      <summary>Copies the values from the specified <see cref="T:System.Web.Http.ModelBinding.ModelStateDictionary" /> object into this dictionary, overwriting existing values if keys are the same.</summary>
      <param name="dictionary">The dictionary.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.Remove(System.Collections.Generic.KeyValuePair{System.String,System.Web.Http.ModelBinding.ModelState})">
      <summary>Removes the first occurrence of the specified object from the model-state dictionary.</summary>
      <returns>true if item was successfully removed the model-state dictionary; otherwise, false. This method also returns false if item is not found in the model-state dictionary.</returns>
      <param name="item">The object to remove from the model-state dictionary.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.Remove(System.String)">
      <summary>Removes the element that has the specified key from the model-state dictionary.</summary>
      <returns>true if the element is successfully removed; otherwise, false. This method also returns false if key was not found in the model-state dictionary.</returns>
      <param name="key">The key of the element to remove.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.SetModelValue(System.String,System.Web.Http.ValueProviders.ValueProviderResult)">
      <summary>Sets the value for the specified key by using the specified value provider dictionary.</summary>
      <param name="key">The key.</param>
      <param name="value">The value.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.System#Collections#IEnumerable#GetEnumerator">
      <summary>Returns an enumerator that iterates through a collection.</summary>
      <returns>An IEnumerator object that can be used to iterate through the collection.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.TryGetValue(System.String,System.Web.Http.ModelBinding.ModelState@)">
      <summary>Attempts to gets the value that is associated with the specified key.</summary>
      <returns>true if the object contains an element that has the specified key; otherwise, false.</returns>
      <param name="key">The key of the value to get.</param>
      <param name="value">The value associated with the specified key.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelStateDictionary.Values">
      <summary>Gets a collection that contains the values in the dictionary.</summary>
      <returns>A collection that contains the values of the model-state dictionary.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Pa