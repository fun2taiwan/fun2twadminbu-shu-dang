"bindingContext">The binding context.</param>
      <param name="newCollection">The new collection.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.ArrayModelBinderProvider">
      <summary>Provides a model binder for arrays.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.ArrayModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.ArrayModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.ArrayModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Returns a model binder for arrays.</summary>
      <returns>A model binder object or null if the attempt to get a model binder is unsuccessful.</returns>
      <param name="configuration">The configuration.</param>
      <param name="modelType">The type of model.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.CollectionModelBinder`1">
      <summary>Maps a browser request to a collection.</summary>
      <typeparam name="TElement">The type of the collection.</typeparam>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CollectionModelBinder`1.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CollectionModelBinder`1" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CollectionModelBinder`1.BindModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Binds the model by using the specified execution context and binding context.</summary>
      <returns>true if model binding is successful; otherwise, false.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CollectionModelBinder`1.CreateOrReplaceCollection(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext,System.Collections.Generic.IList{`0})">
      <summary>Provides a way for derived classes to manipulate the collection before returning it from the binder.</summary>
      <returns>true in all cases.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
      <param name="newCollection">The new collection.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.CollectionModelBinderProvider">
      <summary>Provides a model binder for a collection.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CollectionModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CollectionModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CollectionModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Retrieves a model binder for a collection.</summary>
      <returns>The model binder.</returns>
      <param name="configuration">The configuration of the model.</param>
      <param name="modelType">The type of the model.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.ComplexModelDto">
      <summary>Represents a data transfer object (DTO) for a complex model.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.ComplexModelDto.#ctor(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Metadata.ModelMetadata})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.ComplexModelDto" /> class.</summary>
      <param name="modelMetadata">The model metadata.</param>
      <param name="propertyMetadata">The collection of property metadata.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.Binders.ComplexModelDto.ModelMetadata">
      <summary>Gets or sets the model metadata of the <see cref="T:System.Web.Http.ModelBinding.Binders.ComplexModelDto" />.</summary>
      <returns>The model metadata of the <see cref="T:System.Web.Http.ModelBinding.Binders.ComplexModelDto" />.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.Binders.ComplexModelDto.PropertyMetadata">
      <summary>Gets or sets the collection of property metadata of the <see cref="T:System.Web.Http.ModelBinding.Binders.