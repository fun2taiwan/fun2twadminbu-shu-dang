create this compiled model.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbPropertyValues">
            <summary>
                A collection of all the properties for an underlying entity or complex object.
            </summary>
            <remarks>
                An instance of this class can be converted to an instance of the generic class
                using the Cast method.
                Complex properties in the underlying entity or complex object are represented in
                the property values as nested instances of this class.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyValues.#ctor(System.Data.Entity.Internal.InternalPropertyValues)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbPropertyValues"/> class.
            </summary>
            <param name="internalValues">The internal dictionary.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyValues.ToObject">
            <summary>
                Creates an object of the underlying type for this dictionary and hydrates it with property
                values from this dictionary.
            </summary>
            <returns>The properties of this dictionary copied into a new object.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(System.Object)">
            <summary>
                Sets the values of this dictionary by reading values out of the given object.
                The given object can be of any type.  Any property on the object with a name that
                matches a property name in the dictionary and can be read will be read.  Other
                properties will be ignored.  This allows, for example, copying of properties from
                simple Data Transfer Objects (DTOs).
            </summary>
            <param name = "obj">The object to read values from.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyValues.Clone">
            <summary>
                Creates a new dictionary containing copies of all the properties in this dictionary.
                Changes made to the new dictionary will not be reflected in this dictionary and vice versa.
            </summary>
            <returns>A clone of this dictionary.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyValues.SetValues(System.Data.Entity.Infrastructure.DbPropertyValues)">
            <summary>
                Sets the values of this dictio