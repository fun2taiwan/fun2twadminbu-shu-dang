d(System.Object)">
            <summary>
            A string like "The specified target migration '{0}' does not exist. Ensure that target migration refers to an existing migration id."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.PartialFkOperation(System.Object,System.Object)">
            <summary>
            A string like "The Foreign Key on table '{0}' with columns '{1}' could not be created because the principal key columns could not be determined. Use the AddForeignKey fluent API to fully specify the Foreign Key."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.AutoNotValidTarget(System.Object)">
            <summary>
            A string like "'{0}' is not a valid target migration. When targeting a previously applied automatic migration, use the full migra