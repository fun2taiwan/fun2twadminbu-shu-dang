ing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.DropTableOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.DropTableOperation"/>.
            </summary>
            <param name="dropTableOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.MoveTableOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.MoveTableOperation"/>.
            </summary>
            <param name="moveTableOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.RenameTableOperation,System.Data.Ent