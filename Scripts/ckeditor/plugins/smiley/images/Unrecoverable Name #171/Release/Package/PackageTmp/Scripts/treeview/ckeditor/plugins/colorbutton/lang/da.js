erate(System.Data.Entity.Migrations.Model.RenameTableOperation)">
            <summary>
                Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.RenameTableOperation"/>.
                Generated SQL should be added using the Statement method.
            </summary>
            <param name="renameTableOperation">The operation to produce SQL for.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Generate(System.Data.Entity.Migrations.Model.MoveTableOperation)">
            <summary>
                Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.MoveTableOperation"/>.
                Generated SQL should be added using the Statement method.
            </summary>
            <param name="moveTableOperation">The operation to produce SQL for.</para