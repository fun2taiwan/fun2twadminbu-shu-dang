    <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpActionDescriptor" /> class with specified information that describes the controller of the action.</summary>
      <param name="controllerDescriptor">The information that describes the controller of the action.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.ActionBinding">
      <summary>Gets or sets the binding that describes the action.</summary>
      <returns>The binding that describes the action.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.ActionName">
      <summary>Gets the name of the action.</summary>
      <returns>The name of the action.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.Configuration">
      <summary>Gets or sets the action configuration.</summary>
      <returns>The action configuration.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.ControllerDescriptor">
      <summary>Gets the information that describes the controller of the action.</summary>
      <returns>The information that describes the controller of the action.</returns>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionDescriptor.ExecuteAsync(System.Web.Http.Controllers.HttpControllerContext,System.Collections.Generic.IDictionary{System.String,System.Object},System.Threading.CancellationToken)">
      <summary>Executes the described action and returns a <see cref="T:System.Threading.Tasks.Task`1" /> that once completed will contain the return value of the action.</summary>
      <returns>A <see cref="T:System.Threading.Tasks.Task`1" /> that once completed will contain the return value of the action.</returns>
      <param name="controllerContext">The controller context.</param>
      <param name="arguments">A list of arguments.</param>
      <param name="cancellationToken">The cancellation token.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionDescriptor.GetCustomAttributes``1">
      <summary>Returns the custom attributes associated with the action descriptor.</summary>
      <returns>The custom attributes associated with the action descriptor.</returns>
      <typeparam name="T">The action descriptor.</typeparam>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionDescriptor.GetFilterPipeline">
      <summary>Retrieves the filters for the given configuration and action.</summary>
      <returns>The filters for the given configuration and action.</returns>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionDescriptor.GetFilters">
      <summary>Retrieves the filters for the action descriptor.</summary>
      <returns>The filters for the action descriptor.</returns>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionDescriptor.GetParameters">
      <summary>Retrieves the parameters for the action descriptor.</summary>
      <returns>The parameters for the action descriptor.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.Properties">
      <summary>Gets the properties associated with this instance.</summary>
      <returns>The properties associated with this instance.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.ResultConverter">
      <summary>Gets the converter for correctly transforming the result of calling <see cref="M:System.Web.Http.Controllers.HttpActionDescriptor.ExecuteAsync(System.Web.Http.Controllers.HttpControllerContext,System.Collections.Generic.IDictionary{System.String,System.Object})" />" into an instance of <see cref="T:System.Net.Http.HttpResponseMessage" />.</summary>
      <returns>The action result converter.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.ReturnType">
      <summary>Gets the return type of the descriptor.</summary>
      <returns>The return type of the descriptor.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionDescriptor.SupportedHttpMethods">
      <summary>Gets the collection of supported HTTP methods for the descriptor.</summary>
      <returns>The collection of supported HTTP methods for the descrip