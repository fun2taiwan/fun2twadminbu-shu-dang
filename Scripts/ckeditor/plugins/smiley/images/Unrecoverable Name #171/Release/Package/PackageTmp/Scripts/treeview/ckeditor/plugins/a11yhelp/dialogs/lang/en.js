        Indicates that the version of the <see cref="T:System.Data.Entity.DbModelBuilder"/> and 
                <see cref="T:System.Data.Entity.DbContext"/> conventions shipped with Entity Framework 4.1
                through 4.3 should be used.
            </summary>
        </member>
        <member name="F:System.Data.Entity.DbModelBuilderVersion.V5_0_Net4">
            <summary>
                Indicates that the version of the <see cref="T:System.Data.Entity.DbModelBuilder"/> and 
                <see cref="T:System.Data.Entity.DbContext"/> conventions shipped with Entity Framework 5.0
                when targeting .NET 4 should be used.
            </summary>
        </member>
        <member name="F:System.Data.Entity.DbModelBuilderVersion.V5_0">
            <summary>
                Indicates that the version of the <see cref="T:System.Data.Entity.DbModelBuilder"/> and 
                <see cref="T:System.Data.Entity.DbContext"/> conventions shipped with Entity Framework 5.0
                when targeting .NET 4.5 should be used.
            </summary>
        </member>
        <member name="T:System.Data.Entity.DbModelBuilderVersionAttribute">
            <summary>
                This attribute can be applied to a class derived from <see cref="T:System.Data.Entity.DbContext"/> to set which
                version of the DbContext and <see cref="T:System.Data.Entity.DbModelBuilder"/> conventions should be used when building
                a model from code--also know as "Code First". See the <see cref="T:System.Data.Entity.DbModelBuilderVersion"/>
                enumeration for details about DbModelBuilder versions.
            </summary>
            <remarks>
                If the attribute is missing from DbContextthen DbContext will always use the latest
                version of the conventions.  This is equivalent to using DbModelBuilderVersion.Latest.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilderVersionAttribute.#ctor(System.Data.Entity.DbModelBuilderVersion)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.DbModelBuilderVersionAttribute"/> class.
            </summary>
            <param name="version">The <see cref="T:System.Data.Entity.DbModelBuilder"/> conventions version to use.</param>
        </member>
        <member name="P:System.Data.Entity.DbModelBuilderVersionAttribute.Version">
            <summary>
                Gets the <see cref="T:System.Data.Entity.DbModelBuilder"/> conventions version.
            </summary>
            <value>The <see cref="T:System.Data.Entity.DbModelBuilder"/> conventions version.</value>
        </member>
        <member name="T:System.Data.Entity.DbSet">
            <summary>
                A non-generic version of <see cref="T:System.Data.Entity.DbSet`1"/> which can be used when the type of entity
                is not known at build time.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbQuery">
            <summary>
                Represents a non-generic LINQ to Entities query against a DbContext.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Internal.Linq.IInternalQueryAdapter">
            <summary>
                An internal interface implemented by <see cref="T:System.Data.Entity.Infrastructure.DbQuery`1"/> and <see cref="T:System.Data.Entity.Infrastructure.DbQuery"/> that allows access to
                the internal query without using reflection.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Internal.Linq.IInternalQueryAdapter.InternalQuery">
            <summary>
                The underlying internal set.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery.#ctor">
