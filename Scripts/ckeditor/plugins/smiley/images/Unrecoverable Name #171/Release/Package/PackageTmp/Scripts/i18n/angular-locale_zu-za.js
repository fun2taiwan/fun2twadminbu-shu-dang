           <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbContextConfiguration"/> class.
            </summary>
            <param name="internalContext">The internal context.</param>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextConfiguration.LazyLoadingEnabled">
            <summary>
                Gets or sets a value indicating whether lazy loading of relationships exposed as
                navigation properties is enabled.  Lazy loading is enabled by default.
            </summary>
            <value><c>true</c> if lazy loading is enabled; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextConfiguration.ProxyCreationEnabled">
            <summary>
                Gets or sets a value indicating whether or not the framework will create instances of
                dynamically generated proxy classes whenever it creates an instance of an entity type.
                Note that even if proxy creation is enabled with this flag, proxy instances will only
                be created for entity types that meet the requirements for being proxied.
                Proxy creation is enabled by default.
            </summary>
            <value><c>true</c> if proxy creation is enabled; otherwise, <c>false</c>.</value>
        </member>
        <!-- Badly formed XML comment ignored for member "P:System.Data.Entity.Infrastructure.DbContextConfiguration.AutoDetectChangesEnabled" -->
        <member name="P:System.Data.Entity.Infrastructure.DbContextConfiguration.ValidateOnSaveEnabled">
            <summary>
                Gets or sets a value indicating whether tracked entities should be validated automatically when
                <see cref="M:System.Data.Entity.DbContext.SaveChanges"/> is invoked.
                The