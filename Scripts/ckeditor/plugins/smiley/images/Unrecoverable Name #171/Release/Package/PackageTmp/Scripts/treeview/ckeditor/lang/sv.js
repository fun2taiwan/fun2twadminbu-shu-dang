re.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory">
      <summary>Represents a class that is responsible for creating a new instance of a query-string value-provider object.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Retrieves a value-provider object for the specified controller context.</summary>
      <returns>A query-string value-provider object.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider">
      <summary>Represents a value provider for route data that is contained in an object that implements the IDictionary(Of TKey, TValue) interface.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider.#ctor(System.Web.Http.Controllers.HttpActionContext,System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider" /> class.</summary>
      <param name="actionContext">An object that contain information about the HTTP request.</param>
      <param name="culture">An object that contains information about the target culture.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory">
      <summary>Represents a factory for creating route-data value provider objects.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Retrieves a value-provider object for the specified controller context.</summary>
      <returns>A value-provider object.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
  </members>
</doc>                                                                                                                                                                                                                                                                                                                                                        by using the Ignore method or NotMappedAttribute data annotation. Make sure that it is a valid primitive property._Serializer can only serialize an EdmModel that has one EdmNamespace and one EdmEntityContainer.�Failed to set Database.DefaultConnectionFactory to an instance of the '{0}' type as specified in the application configuration. See inner exception for details.�Direct column renaming is not supported by SQL Server Compact. To rename a column in SQL Server Compact, you will need to recreate it.�The database name '{0}' is not supported because it is an MDF file name. A full connection string must be provided to attach an MDF file.�The StringLengthAttribute on property '{0}' on type '{1}' is not valid. The maximum length must be greater than zero. Use MaxLength() without parameters to indicate that the string or array can have the maximum allowable length.nThe specified table '{0}' was not found in the model. Ensure that the table name has been correctly specified.iThe qualified table name '{���� JFIF  [ Z  �� C 		
 $.' ",#(7),01444'9=82<.342�� C			2!!22222222222222222222222222222222222222222222222222��  � d" ��           	
�� �   } !1AQa"q2���#B��R��$3br�	
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������        	
�� �  w !1AQaq"2�B����	#3R�br�
$4�%�&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������   ? ��C�����#��Ғ�d�n��Ҟ�)���؁��mV_��f=\^�oa���)��"� ct�Oݩ	� ���UGp+4;ص2�h����+�=CL���)��"=)�"��a�T�bքMҢj�����犴bӱW����R���Ӧ���qp9��B*rx�3\׈o��n'���lXC��� ��΢�&�4�l��p�">�ÃJNZ��x�΄[^�y��R"H�r���0�U��*�RB�NV{�E>���"�訢��D)�Zk�!>��c(��z��bH����� gڲ.<s�bC�1Dj���	��'���Zc��\t��F�˴��ῼ�m_͈���nf��gn!|��$~JZ�*n�cw}kfH���2W 3c"�����O-���fH�y�;V�e>-7�O"��1��f�3�6o���3�J��� �9�q�*�uf$�YMk4,�4m��#����_.g�-e`���2Hs��k����c���r0�Fɭ�{Il���ئ������|<'�hUNIk#6�Ǟ#31MR�T� A�����_"Q��G���yjw3�.��4QEfj��O8����ʠ�&�<�v3j��� ��h<m�T��l��&/1�zo�~���q��"�� �9�{����FA>�iY���往�(�ЦԒi�L�i�w#���Xl�dy�
�"��� ץ;�C6�'�K�r�F���pz�EG��6KI<��ٝ�T`~u�v�W��6��?#� n� �?_jο�m��e[�Q���*]ICT���KC�ɂ�h�x��l�����c���� �M���V2oa�a���Oq"3E��Ed�X���P��lꏕ�og��(1Er�[֊+%�ANp6����E��?cD�ʊ(��Lv�#��׏����Ͷ����h�
O�z��l���ם�{]���P�H�W�R��)=����q0�+����ٵ�g�p�T"��}K�a�����6$��ti������,��s`���V��Y���/�SJ-�����d�"���㉔���=sj�f�ќ�u���qY�|J�);a8#��Uۭ��o��"`��*)r���N�%T��.���E#�\D`{�U�t��.v���z
���k�Ǹ�Q�]��� �V�����m��>��x��8MǱu�>e��i�$�
��ڍF=�{mݝ�8�t���J�D���&�ۈna1�j�!�	ʴ�����f5%)|,��X@�Hr�����X�
5ČG}Ɗ���s��˱�f��� }����r��
���Z%���6����ö�6��:�ψ:�Ƌ�yg�ٹ�a����u������e�<�I* �w�_����)��\Bꬎ�����a��eI��I?)�� �P�'-�5�R1��W��n��چ�r>�ҫ� iZC�ə�[mߘ����s�V��Zf�,V����x"�ft��c�Z�#%]�Gj�F��.��E5�f�7��_$
l��'��� �k6��|$�p�jK	���c���pQ7� a���'�@����d�Y`w�d�iMgx<SQL����KBx$���@v$C��x�:�/g�wQ!-��+��?y��%U� 	� y�Y�Z�Cs2Ä�Q6ҴU�l���:�R�%���c3���7�)���7��;r+*�]�y#��x��V[Q�}�������֔m�C�K���2���Q�4Vy�e8�/Q��Tr��U�����>�xQc7r0bP{}+b�Y���'�nM�ͱ�u&2݈��墸��g`á#v+��� �9t���G|��r�0�>��Q;=���4��Yv�cq�/��V��@���P�]q�ڬ麼�ީ=�m�$9  X�՚����3��$mʤ�kX�'w���&��p??_�dx[?b��y��֒�K=I&G�#q�vy�e�z=��$���r���塬c�"�t����I'���#n /�6��7;�Ā,�"d;:֮���Ayu{#)�|�� �b��CJ[�k�P=	<+9NQi#|>VRr{z3�����0���V%��}#	���[���4�r�ߘ�]����>��+�~�K?�XU����$��=���3�_��Ni�*�a� 9�W����[��999��i���l��(:�$~��m-����p@� ����?
�?�>�r�z�%�Ɗ�W�Ӛg�G�׵�'�A,�H���8����n2�N��n���"�4`<�v�����5��}�K��B�h��gJ�4i^�Q"(�.v�ݼ�7W:�p����ʾp�c��+Q�:H륂�V<�ht:�/��*�Ӛ�k�m�;k�@�7 bO�X��-Q�<���+�E��/�n�*M)r��4I�5$�3W�pn�qĶđ$�B>pp3���ty.][�El��k+���n.U����诩�j>e���+|�����i�s=�i*KJ�������� �Q�2�U� ��Y��RZ8ж�:WOw4V�-���-v���?��6��Nٶ�`y�9ت����Ƽ�.��N�R��)�YC1-���K=��NP���c�5���6��M�\Qt�tj-Ǭ��Ȅ�Ҋ�w�ʲ*B�T��4+���[[�弉�	-�b7�R��+j�ķk��S�7c�ؑ�QM�X�ږ��� v�f֯,�(!*"?2�~V���X�UwMJN1�F��v��KWX��ݎ�ϥC7�o����@��`��3�5�|] �%�J����Z�:�Mڣ��0F�e~�t�pQ���㈯��%��{y��x����>�+�IM��v�G<
��u"4��R��s\��.�f����-�-nO3n�qv�V���d:�x���o�+��a�_��1+���#o�ז1k��qd�cq���Q�}�}J��<Y-��EB�+��玔�wZ����֩�����,.���W��m��C0;�F���ڮ��kVв���Nѝ����.4bD�_<��{E{n_�Zu�-�Đc�$~�R�k326�Q�!z~b����7D�H��{��].����{Kۉ�O��$)�yJOZ��7��΢��1�h�.4����s"��Gj*�+�愚�� o���M�~��U�[M��s|��<�t�>aE�:4�?u����-ު�V�Lw���]��5nkIm��;}�1���E���&�4��;V�yA�Ω	��	�<gQ@����.m�X�[~�:q�4���$��e���!c�n=�袜U�|;�\Z鳅|�π�����E�[�-$���������i:v��G|m�w(��*p � �j��V�ge aB�6�*=h��ks�]a,��>j)��1��+&v�F���                                                                                                                                                  /*!
 * Modernizr v2.5.3
 * www.modernizr.com
 *
 * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
 * Available under the BSD and MIT licenses: www.modernizr.com/license/
 */

/*
 * Modernizr tests which native CSS3 and HTML5 features are available in
 * the current UA and makes the results available to you in two ways:
 * as properties on a global Modernizr object, and as classes on the
 * <html> element. This information allows you to progressively enhance
 * your pages with a granular level of control over the experience.
 *
 * Modernizr has an optional (not included) conditional resource loader
 * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
 * To get a build that includes Modernizr.load(), as well as choosing
 * which tests to include, go to www.modernizr.com/download/
 *
 * Authors        Faruk Ates, Paul Irish, Alex Sexton
 * Contributors   Ryan Seddon, Ben Alman
 */

window.Modernizr = (function( window, document, undefined ) {

    var version = '2.5.3',

    Modernizr = {},
    
    // option for enabling the HTML classes to be added
    enableClasses = true,

    docElement = document.documentElement,

    /**
     * Create our "modernizr" element that we do most feature tests on.
     */
    mod = 'modernizr',
    modElem = document.createElement(mod),
    mStyle = modElem.style,

    /**
     * Create the input element for various Web Forms feature tests.
     */
    inputElem = document.createElement('input'),

    smile = ':)',

    toString = {}.toString,

    // List of property values to set for css tests. See ticket #21
    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),

    // Following spec is to expose vendor-specific style properties as:
    //   elem.style.WebkitBorderRadius
    // and the following would be incorrect:
    //   elem.style.webkitBorderRadius

    // Webkit ghosts their properties in lowercase but Opera & Moz do not.
    // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
    //   erik.eae.net/archives/2008/03/10/21.48.10/

    // More here: github.com/Modernizr/Modernizr/issues/issue/21
    omPrefixes = 'Webkit Moz O ms',

    cssomPrefixes = omPrefixes.split(' '),

    domPrefixes = omPrefixes.toLowerCase().split(' '),

    ns = {'svg': 'http://www.w3.org/2000/svg'},

    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, // used in testing loop


    // Inject element with style element and some CSS rules
    injectElementWithStyles = function( rule, callback, nodes, testnames ) {

      var style, ret, node,
          div = document.createElement('div'),
          // After page load injecting a fake body doesn't work so check if body exists
          body = document.body, 
          // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
          fakeBody = body ? body : document.createElement('body');

      if ( parseInt(nodes, 10) ) {
          // In order not to give false positives we c