 name of the derived
                <see cref="T:System.Data.Entity.DbContext"/> class as the container for the conceptual model built by
                Code First.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.ModelContainerConvention.#ctor(System.String)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.ModelContainerConvention"/> class.
            </summary>
            <param name="containerName">The model container name.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.ModelContainerConvention.System#Data#Entity#ModelConfiguration#Conventions#IEdmConvention#Apply(System.Data.Entity.Edm.EdmModel)">
            <summary>
                Applies the convention to the given model.
            </summary>
            <param name = "model">The model.</param>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.ModelNamespaceConvention">
            <summary>
                This <see cref="T:System.Data.Entity.DbModelBuilder"/> convention uses the namespace of the derived
                <see cref="T:System.Data.Entity.DbContext"/> class as the namespace of the conceptual model built by
                Code First.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.ModelNamespaceConvention.#ctor(System.String)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.ModelNamespaceConvention"/> class.
            </summary>
            <param name="modelNamespace">The model namespace.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.ModelNamespaceConvention.System#Data#Entity#ModelConfiguration#Conventions#IEdmConvention#Apply(System.Data.Entity.Edm.EdmModel)">
            <summary>
                Applies the convention to the given model.
            </summary>
            <param name = "model">The model.</param>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.ReplacementDbQueryWrapper`1">
            <summary>
                Instances of this class are used internally to create constant expressions for <see cref="T:System.Data.Objects.ObjectQuery`1"/>
                that are inserted into the expression tree to  replace references to <see cref="T:System.Data.Entity.Infrastructure.DbQuery`1"/>
                and <see cref="T:System.Data.Entity.Infrastructure.DbQuery"/>.
            </summary>
            <typeparam name="TElement">The type of the element.</typeparam>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.ReplacementDbQueryWrapper`1.#ctor(System.Data.Objects.ObjectQuery{`0})">
            <summary>
                Private constructor called by the Create factory method.
            </summary>
            <param name = "query">The query.</param>
     