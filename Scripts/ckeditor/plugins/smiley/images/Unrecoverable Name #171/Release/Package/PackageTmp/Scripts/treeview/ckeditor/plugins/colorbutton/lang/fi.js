 </param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.PrimaryKeyOperation.Table">
            <summary>
                Gets or sets the name of the table that contains the primary key.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.PrimaryKeyOperation.Columns">
            <summary>
                Gets the column(s) that make up the primary key.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.PrimaryKeyOperation.HasDefaultName">
            <summary>
                Gets a value indicating if a specific name has been supplied for this primary key.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.PrimaryKeyOperation.Name">
            <summary>
                Gets or sets the name of this primary key.
                If no name is supplied, a default name will be calculated.
            </summary>
        </member>
   