  <param name = "model">The model that will back this context.</param>
        </member>
        <member name="M:System.Data.Entity.DbContext.#ctor(System.Data.Common.DbConnection,System.Boolean)">
            <summary>
                Constructs a new context instance using the existing connection to connect to a database.
                The connection will not be disposed when the context is disposed.
            </summary>
            <param name = "existingConnection">An existing connection to use for the new context.</param>
            <param name = "contextOwnsConnection">If set to <c>true</c> the connection is disposed when
                the context is disposed, otherwise the caller must dispose the connection.</param>
        </member>
        <member name="M:System.Data.Entity.DbContext.#ctor(System.Data.Common.DbConnection,System.Data.Entity.Infrastructure.DbCompiledModel,System.Boolean)">
            <summary>