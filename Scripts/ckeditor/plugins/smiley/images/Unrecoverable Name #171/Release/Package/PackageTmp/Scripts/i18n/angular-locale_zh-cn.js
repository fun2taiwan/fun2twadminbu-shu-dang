      <typeparam name="TEntity">The type of the entity.</typeparam>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.#ctor(System.Data.Entity.Internal.InternalEntityEntry)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> class.
            </summary>
            <param name="internalEntityEntry">The internal entry.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.GetDatabaseValues">
            <summary>
                Queries the database for copies of the values of the tracked entity as they currently exist in the database.
                Note that changing the values in the returned dictionary will not update the values in the database.
                If the entity is not found in the database then null is returned.
            </summary>
            <returns>The store values.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Reload">
            <summary>
                Reloads the entity from the database overwriting any property values with values from the database.
                The entity will be in the Unchanged state after calling this method.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Reference(System.String)">
            <summary>
                Gets an object that represents the reference (i.e. non-collection) navigation property from this
                entity to another entity.
            </summary>
            <param name = "navigationProperty">The name of the navigation property.</param>
            <returns>An object representing the navigation property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Reference``1(System.String)">
            <summary>
                Gets an object that represents the reference (i.e. non-collection) navigation property from thi