ue.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.XmlNodeConverter.IsNamespaceAttribute(System.String,System.String@)">
            <summary>
            Checks if the attributeName is a namespace attribute.
            </summary>
            <param name="attributeName">Attribute name to test.</param>
            <param name="prefix">The attribute name prefix if it has one, otherwise an empty string.</param>
            <returns>True if attribute name is for a namespace attribute, otherwise false.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.XmlNodeConverter.CanConvert(System.Type)">
            <summary>
            Determines whether this instance can convert the specified value type.
            </summary>
            <param name="valueType">Type of the value.</param>
            <returns>
            	<c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.FloatParseHandling">
            <summary>
            Specifies how floating point numbers, e.g. 1.0 and 9.9, are parsed when reading JSON text.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.FloatParseHandling.Double">
            <summary>
            Floating point numbers are parsed to <see cref="F:Newtonsoft.Json.FloatParseHandling.Double"/>.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.FloatParseHandling.Decimal">
            <summary>
            Floating point numbers are parsed to <see cref="F:Newtonsoft.Json.FloatParseHandling.Decimal"/>.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.DateFormatHandling">
            <summary>
            Specifies how dates are formatted when writing JSON text.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateFormatHandling.IsoDateFormat">
            <summary>
            Dates are written in the ISO 8601 format, e.g. "2012-03-21T05:40Z".
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat">
            <summary>
            Dates are written in the Microsoft JSON format, e.g. "\/Date(1198908717056)\/".
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.DateParseHandling">
            <summary>
            Specifies how date formatted strings, e.g. "\/Date(1198908717056)\/" and "2012-03-21T05:40Z", are parsed when reading JSON text.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateParseHandling.None">
            <summary>
            Date formatted strings are not parsed to a date type and are read as strings.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateParseHandling.DateTime">
            <summary>
            Date formatted strings, e.g. "\/Date(1198908717056)\/" and "2012-03-21T05:40Z", are parsed to <see cref="F:Newtonsoft.Json.DateParseHandling.DateTime"/>.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateParseHandling.DateTimeOffset">
            <summary>
            Date formatted strings, e.g. "\/Date(1198908717056)\/" and "2012-03-21T05:40Z", are parsed to <see cref="F:Newtonsoft.Json.DateParseHandling.DateTimeOffset"/>.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.DateTimeZoneHandling">
            <summary>
            Specifies how to treat the time value when converting between string and <see cref="T:System.DateTime"/>.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateTimeZoneHandling.Local">
            <summary>
            Treat as local time. If the <see cref="T:System.DateTime"/> object represents a Coordinated Universal Time (UTC), it is converted to the local time.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateTimeZoneHandling.Utc">
            <summary>
            Treat as a UTC. If the <see cref="T:System.DateTime"/> object represents a local time, it is converted to a UTC.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateTimeZoneHandling.Unspecified">
            <summary>
            Treat as a local time if a <see cref="T:System.DateTime"/> is being converted to a string.
            If a string is being converted to <see cref="T:System.DateTime"/>, convert to a local time if a time zone is specified.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind">
            <summary>
            Time zone information should be preserved when converting.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.DefaultValueHandling">
            <