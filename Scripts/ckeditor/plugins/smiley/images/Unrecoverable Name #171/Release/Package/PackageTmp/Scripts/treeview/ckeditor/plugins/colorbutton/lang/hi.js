ta.Entity.Migrations.Model.DropPrimaryKeyOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.DropPrimaryKeyOperation"/>.
            </summary>
            <param name="dropPrimaryKeyOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.CreateIndexOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.CreateIndexOperation"/>.
            </summary>
            <param name="createIndexOperation">The operation to generate code for.</param>
       