member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetComponentName">
            <summary>
            Returns the name of this instance of a component.
            </summary>
            <returns>
            The name of the object, or null if the object does not have a name.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetConverter">
            <summary>
            Returns a type converter for this instance of a component.
            </summary>
            <returns>
            A <see cref="T:System.ComponentModel.TypeConverter"/> that is the converter for this object, or null if there is no <see cref="T:System.ComponentModel.TypeConverter"/> for this object.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetDefaultEvent">
            <summary>
            Returns the default event for this instance of a component.
            </summary>
            <returns>
            An <see cref="T:System.ComponentModel.EventDescriptor"/> that represents the default event for this object, or null if this object does not have events.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetDefaultProperty">
            <summary>
            Returns the default property for this instance of a component.
            </summary>
            <returns>
            A <see cref="T:System.ComponentModel.PropertyDescriptor"/> that represents the default property for this object, or null if this object does not have properties.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetEditor(System.Type)">
            <summary>
            Returns an editor of the specified type for this instance of a component.
            </summary>
            <param name="editorBaseType">A <see cref="T:System.Type"/> that represents the editor for this object.</param>
            <returns>
            An <see cref="T:System.Object"/> of the specified type that is the editor for this object, or null if the editor cannot be found.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetEvents(System.Attribute[])">
            <summary>
            Returns the events for this instance of a component using the specified attribute array as a filter.
            </summary>
            <param name="attributes">An array of type <see cref="T:System.Attribute"/> that is used as a filter.</param>
            <returns>
            An <see cref="T:System.ComponentModel.EventDescriptorCollection"/> that represents the filtered events for this component instance.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetEvents">
            <summary>
            Returns the events for this instance of a component.
            </summary>
            <returns>
            An <see cref="T:System.ComponentModel.EventDescriptorCollection"/> that represents the events for this component instance.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.System#ComponentModel#ICustomTypeDescriptor#GetPropertyOwner(System.ComponentModel.PropertyDescriptor)">
            <summary>
            Returns an object that contains the property described by the specified property descriptor.
            </summary>
            <param name="pd">A <see cref="T:System.ComponentModel.PropertyDescriptor"/> that represents the property whose owner is to be found.</param>
            <returns>
            An <see cref="T:System.Object"/> that represents the owner of the specified property.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JObject.GetMetaObject(System.Linq.Expressions.Expression)">
            <summary>
            Returns the <see cref="T:System.Dynamic.DynamicMetaObject"/> responsible for binding operations performed on this object.
            </summary>
            <param name="parameter">The expression tree representation of the runtime value.</param>
            <returns>
            The <see cref="T:System.Dynamic.DynamicMetaObject"/> to bind this object.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.Linq.JsonMergeSettings">
            <summary>
            Specifies the settings used when merging JSON.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JsonMergeSettings.MergeArrayHandling">
            <summary>
            Gets or sets the method used when merging JSON arrays.
            </summary>
            <value>The method used when merging JSON arrays.</valu