            Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "name">The name of the foreign key constraint in the database.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.DbMigration.DropForeignKey(System.String,System.String,System.String,System.String,System.Object)">
            <summary>
                Adds an operation to drop a foreign key constraint based on the column it targets.
            </summary>
            <param name = "dependentTable">
                The table that contains the foreign key column.
                Schema name is optional, if no schema is 