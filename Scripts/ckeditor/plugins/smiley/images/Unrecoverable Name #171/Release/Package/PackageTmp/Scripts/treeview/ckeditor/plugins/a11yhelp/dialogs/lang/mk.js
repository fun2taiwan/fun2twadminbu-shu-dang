    <summary>Gets a value that indicates whether the parameter is optional.</summary>
      <returns>true if the parameter is optional; otherwise, false..</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterDescriptor.ParameterBinderAttribute">
      <summary>Gets or sets the parameter binding attribute.</summary>
      <returns>The parameter binding attribute.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterDescriptor.ParameterName">
      <summary>Gets the name of the parameter.</summary>
      <returns>The name of the parameter.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterDescriptor.ParameterType">
      <summary>Gets the type of the parameter.</summary>
      <returns>The type of the parameter.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterDescriptor.Prefix">
      <summary>Gets the prefix of this parameter.</summary>
      <returns>The prefix of this parameter.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterDescriptor.Properties">
      <summary>Gets the properties of this parameter.</summary>
      <returns>The properties of this parameter.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.IActionResultConverter">
      <summary> A contract for a conversion routine that can take the result of an action returned from &lt;see cref="M:System.Web.Http.Controllers.HttpActionDescriptor.ExecuteAsync(System.Web.Http.Controllers.HttpControllerContext,System.Collections.Generic.IDictionary{System.String,System.Object})" /&gt; and convert it to an instance of <see cref="T:System.Net.Http.HttpResponseMessage" />. </summary>
    </member>
    <member name="M:System.Web.Http.Controllers.IActionResultConverter.Convert(System.Web.Http.Controllers.HttpControllerContext,System.Object)">
      <summary>Converts the specified <see cref="T:System.Web.Http.Controllers.IActionResultConverter" /> object to another object.</summary>
      <returns>The converted object.</returns>
      <param name="controllerContext">The controller context.</param>
      <param name="actionResult">The action result.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.IActionValueBinder">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.IActionValueBinder.GetBinding(System.Web.Http.Controllers.HttpActionDescriptor)">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.HttpActionBinding" /></summary>
      <returns>A <see cref="T:System.Web.Http.Controllers.HttpActionBinding" /> object.</returns>
      <param name="actionDescriptor">The action descriptor.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.IControllerConfiguration">
      <summary> If a controller is decorated with an attribute with this interface, then it gets invoked to initialize the controller settings.  </summary>
    </member>
    <member name="M:System.Web.Http.Controllers.IControllerConfiguration.Initialize(System.Web.Http.Controllers.HttpControllerSettings,System.Web.Http.Controllers.HttpControllerDescriptor)">
      <summary> Callback invoked to set per-controller overrides for this controllerDescriptor. </summary>
      <param name="controllerSettings">The controller settings to initialize.</param>
      <param name="controllerDescriptor">The controller descriptor. Note that the <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> can be associated with the derived controller type given that <see cref="T:System.Web.Http.Controllers.IControllerConfiguration" /> is inherited.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.IHttpActionInvoker">
      <summary>Contains method that is used to invoke HTTP operation.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.IHttpActionInvoker.InvokeActionAsync(System.Web.Http.Controllers.HttpActionContext,