Controllers.HttpParameterBinding[])">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpActionBinding" /> class.</summary>
      <param name="actionDescriptor">The back pointer to the action this binding is for.  </param>
      <param name="bindings">The synchronous bindings for each parameter.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionBinding.ActionDescriptor">
      <summary>Gets or sets the back pointer to the action this binding is for.  </summary>
      <returns>The back pointer to the action this binding is for.  </returns>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionBinding.ExecuteBindingAsync(System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Executes asynchronously the binding for the given request context.</summary>
      <returns>Task that is signaled when the binding is complete. </returns>
      <param name="actionContext">The action context for the binding. This contains the parameter dictionary that will get populated.</param>
      <param name="cancellationToken">The cancellation token for cancelling the binding operation. Or a binder can also bind a parameter to this.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionBinding.ParameterBindings">
      <summary>Gets or sets the synchronous bindings for each parameter. </summary>
      <returns>The synchronous bindings for each parameter.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.HttpActionContext">
      <summary>Contains information for the executing action.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionContext.#ctor">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpActionContext" /> class. </summary>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionContext.#ctor(System.Web.Http.Controllers.HttpControllerContext,System.Web.Http.Controllers.HttpActionDescriptor)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpActionContext" /> class.</summary>
      <param name="controllerContext">The controller context.</param>
      <param name="actionDescriptor">The action descriptor.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.ActionArguments">
      <summary>Gets a list of action arguments.</summary>
      <returns>A list of action arguments.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.ActionDescriptor">
      <summary>Gets or sets the action descriptor for the action context.</summary>
      <returns>The action descriptor.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.ControllerContext">
      <summary>Gets or sets the controller context.</summary>
      <returns>The controller context.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.ModelState">
      <summary>Gets the model state dictionary for the context.</summary>
      <returns>The model state dictionary.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.Request">
      <summary>Gets the request message for the action context.</summary>
      <returns>The request message for the action context.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpActionContext.Response">
      <summary>Gets or sets the response message for the action context.</summary>
      <returns>The response message for the action context.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.HttpActionContextExtensions">
      <summary>Contains extension methods for <see cref="T:System.Web.Http.Controllers.HttpActionContext" />.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpActionContextExtensions.Bind(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Binds the model to a value by using the specified controller context and binding context.</summary>
    