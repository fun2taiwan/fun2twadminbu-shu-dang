onnection">Information used to create a DbConnection.</param>
            <param name = "operation">The operation to perform.</param>
            <returns>The return value of the operation.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.PerformDatabaseOp(System.Data.Common.DbConnection,System.Func{System.Data.Objects.ObjectContext,System.Boolean})">
            <summary>
                Performs the operation defined by the given delegate against a connection.  The connection
                is either the connection accessed from the context backing this object, or is obtained from
                the connection information passed to one of the static methods.
            </summary>
            <param name = "connection">The connection to use.</param>
            <param name = "operation">The operation to perform.</param>
            <returns>The return value of the operation.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.CreateEmptyObjectContext(System.Data.Common.DbConnection)">
            <summary>
                Returns an empty ObjectContext that can be used to perform delete/exists operations.
            </summary>
            <param name = "connection">The connection for which to create an ObjectContext</param>
            <returns>The empty context.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.SqlQuery``1(System.String,System.Object[])">
            <summary>
                Creates a raw SQL query that will return elements of the given generic type.
                The type can be any type that has properties that match the names of the columns returned
                from the query, or can be a simple primitive type.  The type does not have to be an
                entity type. The results of this query are never tracked by the context even if the
                type of object returned is an entity type.  Use the <see cref="M:System.Data.Entity.DbSet`1.SqlQuery(System.String,System.Object[])"/>
                method to return entities that are tracked by the context.
            </summary>
            <typeparam name="TElement">The type of object returned by the query.</typeparam>
            <param name="sql">The SQL query string.</param>
            <param name="parameters">The parameters to apply to the SQL query string.</param>
            <returns>A <see cref="T:System.Collections.Generic.IEnumerable`1"/> object that will execute the query when it is enumerated.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.SqlQuery(System.Type,System.String,System.Object[])">
            <summary>
                Creates a raw SQL query that will return elements of the given type.
                The type can be any type that has properties that match the names of the columns returned
                from the query, or can be a simple primitive type.  The type does not have to be an
                entity type. The results of this query are never tracked by the context even if the
                type of object returned is an entity type.  Use the <see cref="M:System.Data.Entity.DbSet.SqlQuery(System.String,System.Object[])"/>
                method to return entities that are tracked by the context.
            </summary>
            <param name="elementType">The type of object returned by the query.</param>
            <param name="sql">The SQL query string.</param>
            <param name="parameters">The parameters to apply to the SQL query string.</param>
            <returns>A <see cref="T:System.Collections.IEnumerable"/> object that will execute the query when it is enumerated.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.ExecuteSqlCommand(System.String,System.Object[])">
            <summary>
                Executes the given DDL/DML command against the database.
          