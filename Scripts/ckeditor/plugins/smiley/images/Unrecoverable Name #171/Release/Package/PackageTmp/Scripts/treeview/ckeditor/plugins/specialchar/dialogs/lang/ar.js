      Gets or sets the type name handling used when serializing the collection's items.
            </summary>
            <value>The type name handling.</value>
        </member>
        <member name="M:Newtonsoft.Json.JsonContainerAttribute.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonContainerAttribute"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonContainerAttribute.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonContainerAttribute"/> class with the specified container Id.
            </summary>
            <param name="id">The container Id.</param>
        </member>
        <member name="T:Newtonsoft.Json.JsonConvert">
            <summary>
            Provides methods for converting between common language runtime types and JSON types.
            </summary>
            <example>
              <code lang="cs" source="..\Src\Newtonsoft.Json.Tests\Documentation\SerializationTests.cs" region="SerializeObject" title="Serializing and Deserializing JSON with JsonConvert" />
            </example>
        </member>
        <member name="P:Newtonsoft.Json.JsonConvert.DefaultSettings">
            <summary>
            Gets or sets a function that creates default <see cref="T:Newtonsoft.Json.JsonSerializerSettings"/>.
            Default settings are automatically used by serialization methods on <see cref="T:Newtonsoft.Json.JsonConvert"/>,
            and <see cref="M:Newtonsoft.Json.Linq.JToken.ToObject``1"/> and <see cref="M:Newtonsoft.Json.Linq.JToken.FromObject(System.Object)"/> on <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            To serialize without using any default settings create a <see cref="T:Newtonsoft.Json.JsonSerializer"/> with
            <see cref="M:Newtonsoft.Json.JsonSerializer.Create"/>.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.True">
            <summary>
            Represents JavaScript's boolean value true as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.False">
            <summary>
            Represents JavaScript's boolean value false as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.Null">
            <summary>
            Represents JavaScript's null as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.Undefined">
            <summary>
            Represents JavaScript's undefined as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.PositiveInfinity">
            <summary>
            Represents JavaScript's positive infinity as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.NegativeInfinity">
            <summary>
            Represents JavaScript's negative infinity as a string. This field is read-only.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.JsonConvert.NaN">
            <summary>
            Represents JavaScript's NaN as a string. This field is read-only.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.DateTime)">
            <summary>
            Converts the <see cref="T:System.DateTime"/> to its JSON string representation.
            </summary>
            <param name="value">The value to convert.</param>
            <returns>A JSON string representation of the <see cref="T:System.DateTime"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.DateTime,Newtonsoft.Json.DateFormatHandling,Newtonsoft.Json.DateTimeZoneHandling)">
            <summary>
            Converts the <see cref="T:System.DateTime"/> to its JSON string representation using the <see cref="T:Newtonsoft.Json.DateFormatHandling"/> specified.
            </summary>
            <param name="value">The value to convert.</param>
            <param name="format">The format the date will be converted to.</param>
            <param name="timeZoneHandling">The time zone handling when the date is converted to a string.</param>
            <returns>A JSON string representation of the <see cref="T:System.DateTime"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonConvert.ToString(System.DateTimeOffset)">
        