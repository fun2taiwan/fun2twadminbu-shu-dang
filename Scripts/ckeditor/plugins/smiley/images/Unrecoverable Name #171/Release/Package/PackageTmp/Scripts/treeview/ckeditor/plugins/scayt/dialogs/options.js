d">
            <summary>
            Reads the next JSON token from the stream.
            </summary>
            <returns>true if the next token was read successfully; false if there are no more tokens to read.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsInt32">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsString">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.String"/>.
            </summary>
            <returns>A <see cref="T:System.String"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsBytes">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Byte"/>[].
            </summary>
            <returns>A <see cref="T:System.Byte"/>[] or a null reference if the next JSON token is null. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsDouble">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsBoolean">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsDecimal">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsDateTime">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.ReadAsDateTimeOffset">
            <summary>
            Reads the next JSON token from the stream as a <see cref="T:System.Nullable`1"/>.
            </summary>
            <returns>A <see cref="T:System.Nullable`1"/>. This method will return <c>null</c> at the end of an array.</returns>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.Skip">
            <summary>
            Skips the children of the current token.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.SetToken(Newtonsoft.Json.JsonToken)">
            <summary>
            Sets the current token.
            </summary>
            <param name="newToken">The new token.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.SetToken(Newtonsoft.Json.JsonToken,System.Object)">
            <summary>
            Sets the current token and value.
            </summary>
            <param name="newToken">The new token.</param>
            <param name="value">The value.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.SetStateBasedOnCurrent">
            <summary>
            Sets the state based on current token type.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.System#IDisposable#Dispose">
            <summary>
            Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.Dispose(System.Boolean)">
            <summary>
            Releases unmanaged and - optionally - managed resources
            </summary>
            <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonReader.Close">
            <summary>
            Changes the <see cref="T:Newtonsoft.Json.JsonReader.State"/> to Closed. 
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.JsonReaderException">
            <summary>
            The exception thrown when an error occurs while reading JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonReaderException.LineNumber">
            <summary>
            Gets the line number indicating where the error occurred.
            </summary>
            <value>The line number indicating where the error occurred.</value>
        </member>
        <member name="P:Newtonsoft.Json.JsonReaderException.LinePosition">
            <summary>
            Gets the line position indicating where the error occurred.
            </summary>
            <value>The line position indicating where the error occurred.</value>
        </member>
        <member name="P:Newtonsoft.Json.JsonReaderException.Path">
            <summary>
            Gets the path to the JSON where the error occurred.
            </summary>
            <value>The path to the JSON where the error occurred.</value>
        </member>
        <member name="M:Newtonsoft.Json.JsonReaderException.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonReaderException"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonReaderException.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonReaderException"/> class
            with a specified error message.
            </summary>
            <param name="message">The error message that explains the reason for the exception.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonReaderException.#ctor(System.String,System.Exception)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonReaderException"/> class
            with a specified error message and a reference to the inner exception that is the cause of this exception.
            </summary>
            <param name="message">The error message that explains the reason for the exception.</param>
            <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonReaderException.#ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonReaderException"/> class.
            </summary>
            <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
            <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
            <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
            <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
        </member>
        <member name="T:Newtonsoft.Json.JsonRequiredAttribute">
            <summary>
            Instructs the <see cref="T:Newtonsoft.Json.JsonSerializer"/> to always serialize the member, and require the member has a value.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.JsonSerializationException">
            <su