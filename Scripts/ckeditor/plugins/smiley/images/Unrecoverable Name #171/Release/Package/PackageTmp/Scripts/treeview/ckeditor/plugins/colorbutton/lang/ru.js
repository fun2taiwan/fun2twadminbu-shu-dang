aultValue">Constant value to use as the default value for this column.</param>
            <param name = "defaultValueSql">SQL expression used as the default value for this column.</param>
            <param name = "name">The name of the column.</param>
            <param name = "storeType">Provider specific data type to use for this column.</param>
            <returns>The newly constructed column definition.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Builders.ColumnBuilder.Geometry(System.Nullable{System.Boolean},System.Data.Spatial.DbGeometry,System.String,System.String,System.String)">
            <summary>
                Creates a new column definition to store geometry data.
            </summary>
            <param name = "nullable">Value indicating whether or not the column allows null values.</param>
            <param name = "defaultValue">Constant value to use as the default value for this column.</param>
            <param name = "defaultValueSql">SQL expression used as the default value for this column.</param>
            <param name = "name">The name of the column.</param>
            <param name = "storeType">Provider specific data type to use for this column.</param>
            <returns>The newly constructed column definition.</returns>
        </member>
        <member name="T:System.Data.Entity.Migrations.Builders.TableBuilder`1">
            <summary>
                He