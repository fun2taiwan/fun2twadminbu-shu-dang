am name="configuration">The configuration.</param>
      <param name="modelType">model type that the binder is expected to bind.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelBinderAttribute.GetModelBinderProvider(System.Web.Http.HttpConfiguration)">
      <summary>Gets the model binder provider.</summary>
      <returns>The <see cref="T:System.Web.Http.ModelBinding.ModelBinderProvider" /> instance.</returns>
      <param name="configuration">The <see cref="T:System.Web.Http.HttpConfiguration" /> configuration object.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelBinderAttribute.GetValueProviderFactories(System.Web.Http.HttpConfiguration)">
      <summary> Gets the value providers that will be fed to the model binder. </summary>
      <returns>A collection of <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> instances.</returns>
      <param name="configuration">The <see cref="T:System.Web.Http.HttpConfiguration" /> configuration object.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderAttribute.Name">
      <summary>Gets or sets the name to consider as the parameter name during model binding.</summary>
      <returns>The parameter name to consider.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderAttribute.SuppressPrefixCheck">
      <summary>Gets or sets a value that specifies whether the prefix check should be suppressed.</summary>
      <returns>true if the prefix check should be suppressed; otherwise, false.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelBinderConfig">
      <summary>Provides a container for model-binder configuration.</summary>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderConfig.ResourceClassKey">
      <summary>Gets or sets the name of the resource file (class key) that contains localized string values.</summary>
      <returns>The name of the resource file (class key).</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderConfig.TypeConversionErrorMessageProvider">
      <summary>Gets or sets the current provider for type-conversion error message.</summary>
      <returns>The current provider for type-conversion error message.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderConfig.ValueRequiredErrorMessageProvider">
      <summary>Gets or sets the current provider for value-required error messages.</summary>
      <returns>The error message provider.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelBinderErrorMessageProvider">
      <summary>Provides a container for model-binder error message provider.</summary>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelBinderParameterBinding">
      <summary> Describes a parameter that gets bound via ModelBinding.   </summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelBinderParameterBinding.#ctor(System.Web.Http.Controllers.HttpParameterDescriptor,System.Web.Http.ModelBinding.IModelBinder,System.Collections.Generic.IEnumerable{System.Web.Http.ValueProviders.ValueProviderFactory})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelBinderParameterBinding" /> class.</summary>
      <param name="descriptor">The parameter descriptor.</param>
      <param name="modelBinder">The model binder.</param>
      <param name="valueProviderFactories">The collection of value provider factory.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBinderParameterBinding.Binder">
      <summary>Gets the model binder.</summary>
      <returns>The model binder.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelBinderParameterBinding.ExecuteBindingAsync(System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Asynchronously executes the parameter binding via the model binder.</summary>
      <returns>The task that is signaled when the binding is complete.</returns>
      <param name="metadataProvider">The metadata provider to use for validation.</param>
      <param name="actionContext">The action context for the binding.</param>
      <param name="cancellationToken">The cancellation token assigned for this task for cancelling the binding operation.</param>
    </member>
    <member name="P:System.Web.Http