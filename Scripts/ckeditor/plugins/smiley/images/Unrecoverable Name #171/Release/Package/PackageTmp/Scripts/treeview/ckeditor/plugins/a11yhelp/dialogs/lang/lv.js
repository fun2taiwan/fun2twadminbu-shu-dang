llers.HttpParameterDescriptor,System.Collections.Generic.IEnumerable{System.Net.Http.Formatting.MediaTypeFormatter})">
      <summary>Binds parameter by parsing the HTTP body content.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="formatters">The list of formatters which provides selection of an appropriate formatter for serializing the parameter into object.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithFormatter(System.Web.Http.Controllers.HttpParameterDescriptor,System.Collections.Generic.IEnumerable{System.Net.Http.Formatting.MediaTypeFormatter},System.Web.Http.Validation.IBodyModelValidator)">
      <summary>Binds parameter by parsing the HTTP body content.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="formatters">The list of formatters which provides selection of an appropriate formatter for serializing the parameter into object.</param>
      <param name="bodyModelValidator">The body model validator used to validate the parameter.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithFormatter(System.Web.Http.Controllers.HttpParameterDescriptor,System.Net.Http.Formatting.MediaTypeFormatter[])">
      <summary>Binds parameter by parsing the HTTP body content.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="formatters">The list of formatters which provides selection of an appropriate formatter for serializing the parameter into object.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithModelBinding(System.Web.Http.Controllers.HttpParameterDescriptor)">
      <summary>Binds parameter by parsing the query string.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithModelBinding(System.Web.Http.Controllers.HttpParameterDescriptor,System.Collections.Generic.IEnumerable{System.Web.Http.ValueProviders.ValueProviderFactory})">
      <summary>Binds parameter by parsing the query string.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="valueProviderFactories">The value provider factories which provide query string parameter data.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithModelBinding(System.Web.Http.Controllers.HttpParameterDescriptor,System.Web.Http.ModelBinding.IModelBinder)">
      <summary>Binds parameter by parsing the query string.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="binder">The model binder used to assemble the parameter into an object.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithModelBinding(System.Web.Http.Controllers.HttpParameterDescriptor,System.Web.Http.ModelBinding.IModelBinder,System.Collections.Generic.IEnumerable{System.Web.Http.ValueProviders.ValueProviderFactory})">
      <summary>Binds parameter by parsing the query string.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.</param>
      <param name="binder">The model binder used to assemble the parameter into an object.</param>
      <param name="valueProviderFactories">The value provider factories which provide query string parameter data.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ParameterBindingExtensions.BindWithModelBinding(System.Web.Http.Controllers.HttpParameterDescriptor,System.Web.Http.ValueProviders.ValueProviderFactory[])">
      <summary>Binds parameter by parsing the query string.</summary>
      <returns>The HTTP parameter binding object.</returns>
      <param name="parameter">The parameter descriptor that describes the parameter to bind.