tionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Binds the model by using the specified execution context and binding context.</summary>
      <returns>true if model binding is successful; otherwise, false.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.TypeMatchModelBinderProvider">
      <summary>Provides a model binder for a model that does not require type conversion.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.TypeMatchModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.TypeMatchModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.TypeMatchModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Retrieves the associated model binder.</summary>
      <returns>The associated model binder.</returns>
      <param name="configuration">The configuration.</param>
      <param name="modelType">The type of model.</param>
    </member>
    <member name="T:System.Web.Http.Query.DefaultStructuredQueryBuilder">
      <summary> The <see cref="T:System.Web.Http.Query.DefaultStructuredQueryBuilder" /> understands $filter, $orderby, $top and $skip OData query parameters </summary>
    </member>
    <member name="M:System.Web.Http.Query.DefaultStructuredQueryBuilder.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Query.DefaultStructuredQueryBuilder" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Query.DefaultStructuredQueryBuilder.GetStructuredQuery(System.Uri)">
      <summary> Build the <see cref="T:System.Web.Http.Query.StructuredQuery" /> for the given uri. </summary>
      <returns>The <see cref="T:System.Web.Http.Query.StructuredQuery" /></returns>
      <param name="uri">The <see cref="T:System.Uri" /> to build the <see cref="T:System.Web.Http.Query.StructuredQuery" /> from</param>
    </member>
    <member name="T:System.Web.Http.Query.IStructuredQueryBuilder">
      <summary> A <see cref="T:System.Web.Http.Query.IStructuredQueryBuilder" /> is used to extract the query from a Uri. </summary>
    </member>
    <member name="M:System.Web.Http.Query.IStructuredQueryBuilder.GetStructuredQuery(System.Uri)">
      <summary> Build the <see cref="T:System.Web.Http.Query.StructuredQuery" /> for the given uri. Return null if there is no query  in the Uri. </summary>
      <returns>The <see cref="T:System.Web.Http.Query.StructuredQuery" /></returns>
      <param name="uri">The <see cref="T:System.Uri" /> to build the <see cref="T:System.Web.Http.Query.StructuredQuery" /> from</param>
    </member>
    <member name="T:System.Web.Http.Query.IStructuredQueryPart">
      <summary> Represents a query option like $filter, $top etc. </summary>
    </member>
    <member name="M:System.Web.Http.Query.IStructuredQueryPart.ApplyTo(System.Linq.IQueryable)">
      <summary> Applies this <see cref="T:System.Web.Http.Query.IStructuredQueryPart" /> on to an <see cref="T:System.Linq.IQueryable" /> returning the resultant <see cref="T:System.Linq.IQueryable" /></summary>
      <returns>The resultant <see cref="T:System.Linq.IQueryable" /></returns>
      <param name="source">The source <see cref="T:System.Linq.IQueryable" /></param>
    </member>
    <member name="P:System.Web.Http.Query.IStructuredQueryPart.QueryExpression">
      <summary> The value part of the query parameter for this query part. </summary>
    </member>
    <member name="P:System.Web.Http.Query.IStructuredQueryPart.QueryOperator">
      <summary> The query operator that this query parameter is for. </summary>
    </member>
    <member name="T:System.Web.Http.Query.StructuredQuery">
      <summary> Represents an <see cref="T:System.Linq.IQueryable" />. </summary>
    </member>
    <member name="M:System.Web.Http.Query.StructuredQuery.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Query.Str