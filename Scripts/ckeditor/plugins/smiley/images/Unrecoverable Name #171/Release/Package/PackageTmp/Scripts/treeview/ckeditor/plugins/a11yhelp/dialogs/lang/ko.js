h the new elements should be inserted. If <see cref="F:System.Int32.MaxValue" /> is passed, ensures the elements are added to the end.</param>
      <param name="services">The collection of services to insert.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.IsSingleService(System.Type)">
      <summary> Determine whether the service type should be fetched with GetService or GetServices.  </summary>
      <returns>true iff the service is singular. </returns>
      <param name="serviceType">type of service to query</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.Remove(System.Type,System.Object)">
      <summary> Removes the first occurrence of the given service from the service list for the given service type. </summary>
      <returns>true if the item is successfully removed; otherwise, false.</returns>
      <param name="serviceType">The service type.</param>
      <param name="service">The service instance to remove.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.RemoveAll(System.Type,System.Predicate{System.Object})">
      <summary> Removes all the elements that match the conditions defined by the specified predicate. </summary>
      <returns>The number of elements removed from the list.</returns>
      <param name="serviceType">The service type.</param>
      <param name="match">The delegate that defines the conditions of the elements to remove.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.RemoveAt(System.Type,System.Int32)">
      <summary> Removes the service at the specified index. </summary>
      <param name="serviceType">The service type.</param>
      <param name="index">The zero-based index of the service to remove.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.Replace(System.Type,System.Object)">
      <summary> Replaces all existing services for the given service type with the given service instance. This works for both singular and plural services.  </summary>
      <param name="serviceType">The service type.</param>
      <param name="service">The service instance.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ReplaceMultiple(System.Type,System.Object)">
      <summary>Replaces all instances of a multi-instance service with a new instance.</summary>
      <param name="serviceType">The type of service.</param>
      <param name="service">The service instance that will replace the current services of this type.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ReplaceRange(System.Type,System.Collections.Generic.IEnumerable{System.Object})">
      <summary> Replaces all existing services for the given service type with the given service instances. </summary>
      <param name="serviceType">The service type.</param>
      <param name="services">The service instances.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ReplaceSingle(System.Type,System.Object)">
      <summary>Replaces a single-instance service of a specified type.</summary>
      <param name="serviceType">The service type. </param>
      <param name="service">The service instance.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ResetCache(System.Type)">
      <summary>Removes the cached values for a single service type.</summary>
      <param name="serviceType">The service type.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.ValueResultConverter`1">
      <summary> A converter for creating responses from actions that return an arbitrary <paramref name="T" /> value. </summary>
      <typeparam name="T">The declared return type of an action.</typeparam>
    </member>
    <member name="M:System.Web.Http.Controllers.ValueResultConverter`1.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ValueResultConverter`1" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ValueResultConverter`1.Convert(System.Web.Http.Controllers.HttpControllerContext,System.Object)">
      <summary>Converts the result of an action with arbitrary return type <paramref name="T" /> to an instance of <see cref="T:System.Net.Http.HttpResponseMessage" />.</summary>
      <returns>The newly created <see cref="T:System.Net.Http.HttpResponseMessage" /> object.</returns>
      <param name="controllerContext">The action controller context.</param>
      <param name="actionResult">The execution result.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.VoidResultConverter">
      <summary>Represents a converter for creating a response from actions that do not return a value.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.VoidResultConverter.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.VoidResultConverter" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.VoidResultConverter.Convert(System.Web.Http.Controllers.HttpControllerContext,System.Object)">
      <summary>Converts the created response from actions that do not return a value.</summary>
      <returns>The converted response.</returns>
      <param name="controllerContext">The context of the controller.</param>
      <param name="actionResult">The result of the action.</param>