oBindingList on this property
            </remarks>
            <value>The local view.</value>
        </member>
        <member name="P:System.Data.Entity.DbSet.System#Data#Entity#Internal#Linq#IInternalSetAdapter#InternalSet">
            <summary>
                The internal IQueryable that is backing this DbQuery
            </summary>
        </member>
        <member name="P:System.Data.Entity.DbSet.InternalSet">
            <summary>
                Gets the underlying internal set.
            </summary>
            <value>The internal set.</value>
        </member>
        <member name="T:System.Data.Entity.DbSet`1">
            <summary>
                A DbSet represents the collection of all entities in the context, or that can be queried from the
                database, of a given type.  DbSet objects are created from a DbContext using the DbContext.Set method.
            </summary>
            <remarks>
                Note that DbSet does not support MEST (Multiple Entity Sets per Type) meaning that there is always a
                one-to-one correlation between a type and a set.
            </remarks>
            <typeparam name = "TEntity">The type that defines the set.</typeparam>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbQuery`1">
            <summary>
                Represents a LINQ to Entities query against a DbContext.
            </summary>
            <typeparam name = "TResult">The type of entity to query for.</typeparam>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.#ctor(System.Data.Entity.Internal.Linq.IInternalQuery{`0})">
            <summary>
                Creates a new query that will be backed by the given internal query object.
            </summary>
            <param name = "internalQuery">The backing query.</param>
        </member>
        <!-- Badly formed XML comment ignored for member "M:System.Data.Entity.Infrastructure.DbQuery`1.Include(System.String)" -->
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.AsNoTracking">
            <summary>
                Returns a new query where the entities returned will not be cached in the <see cref="T:System.Data.Entity.DbContext"/>.
            </summary>
            <returns> A new query with NoTracking applied.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.System#ComponentModel#IListSource#GetList">
            <summary>
                Throws an exception indicating that binding directly to a store query is not supported.
                Instead populate a DbSet with data, for example by using the Load extension method, and
                then bind to local data.  For WPF bind to DbSet.Local.  For Windows Forms bind to
                DbSet.Local.ToBindingList().
            </summary>
            <returns>
                Never returns; always throws.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.System#Collections#Generic#IEnumerable{TResult}#GetEnumerator">
            <summary>
                Gets the enumeration of this query causing it to be executed against the store.
            </summary>
            <returns>An enumerator for the query</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.System#Collections#IEnumerable#GetEnumerator">
            <summary>
                Gets the enumeration of this query causing it to be executed against the store.
            </summary>
            <returns>An enumerator for the query</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbQuery`1.ToString">
            <summary>
                Returns a <see cref="T:System.String"/> representation of the underlying query.
            </summary>
            <returns>
    