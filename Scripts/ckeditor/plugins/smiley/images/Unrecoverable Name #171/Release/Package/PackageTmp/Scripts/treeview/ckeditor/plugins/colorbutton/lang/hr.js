f a <see cref="T:System.Data.Entity.Migrations.Model.CreateTableOperation"/>.
            </summary>
            <param name="addPrimaryKeyOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.GenerateInline(System.Data.Entity.Migrations.Model.AddForeignKeyOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform an <see cref="T:System.Data.Entity.Migrations.Model.AddForeignKeyOperation"/> as part of a <see cref="T:System.Data.Entity.Migrations.Model.CreateTableOperation"/>.
            </summary>
            <param name="addForeignKeyOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the 