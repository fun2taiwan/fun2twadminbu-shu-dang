  <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.AutomaticDataLossException.#ctor(System.String)">
            <summary>
                Initializes a new instance of the AutomaticDataLossException class.
            </summary>
            <param name = "message">The message that describes the error.</param>
        </member>
        <member name="T:System.Data.Entity.Migrations.Infrastructure.AutomaticMigrationsDisabledException">
            <summary>
                Represents an error that occurs when there are pending model changes after applying the last migration and automatic migration is disabled.
            </summary>
        </member>
        <member name="M:System.Data.Entity.M