r">The calling serializer.</param>
            <returns>The object value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.RegexConverter.CanConvert(System.Type)">
            <summary>
            Determines whether this instance can convert the specified object type.
            </summary>
            <param name="objectType">Type of the object.</param>
            <returns>
            	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.Converters.StringEnumConverter">
            <summary>
            Converts an <see cref="T:System.Enum"/> to and from its name string value.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Converters.StringEnumConverter.CamelCaseText">
            <summary>
            Gets or sets a value indicating whether the written enum text should be camel case.
            </summary>
            <value><c>true</c> if the written enum text will be camel case; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:Newtonsoft.Json.Converters.StringEnumConverter.AllowIntegerValues">
            <summary>
            Gets or sets a value indicating whether integer values are allowed.
            </summary>
            <value><c>true</c> if integers are allowed; otherwise, <c>false</c>.</value>
        </member>
        <member name="M:Newtonsoft.Json.Converters.StringEnumConverter.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Converters.StringEnumConverter"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Converters.StringEnumConverter.#ctor(System.Boolean)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Converters.StringEnumConverter"/> class.
            </summary>
            <param name="camelCaseText"><c>true</c> if the written enum text will be camel case; otherwise, <c>false</c>.</param>
        </member>
        <member name="M:Newtonsoft.Json.Converters.StringEnumConverter.WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Writes the JSON representation of the object.
            </summary>
            <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param>
            <param name="value">The value.</param>
            <param name="serializer">The calling serializer.</param>
        </member>
        <member name="M:Newtonsoft.Json.Converters.StringEnumConverter.ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Reads the JSON representation of the object.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
            <param name="objectType">Type of the object.</param>
            <param name="existingValue">The existing value of object being read.</param>
            <param name="serializer">The calling serializer.</param>
            <returns>The object value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.StringEnumConverter.CanConvert(System.Type)">
            <summary>
            Determines whether this instance can convert the specified object type.
            </summary>
            <param name="objectType">Type of the object.</param>
            <returns>
            <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.Converters.VersionConverter">
            <summary>
            Converts a <see cref="T:System.Version"/> to and from a string (e.g. "1.2.3.4").
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Converters.VersionConverter.WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Writes the JSON representation of the object.
            </summary>
            <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param>
            <param name="value">The value.</param>
            <param name="serializer">The calling serializer.</param>
        </member>
        <member name="M:Newtonsoft.Json.Converters.VersionConverter.ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Reads the JSON representation of the object.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
            <param name="objectType">Type of the objec