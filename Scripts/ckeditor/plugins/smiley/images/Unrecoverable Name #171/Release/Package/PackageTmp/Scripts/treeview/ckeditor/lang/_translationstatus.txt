       </summary>
            <param name = "entity">The entity to remove.</param>
            <returns>The entity.</returns>
            <remarks>
                Note that if the entity exists in the context in the Added state, then this method
                will cause it to be detached from the context.  This is because an Added entity is assumed not to
                exist in the database such that trying to delete it does not make sense.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbSet`1.Create">
            <summary>
                Creates a new instance of an entity for the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxies and the entity type meets the requirements for creating a proxy.
            </summary>
            <returns>The entity instance, which may be a proxy.</returns>
        </member>
        <member name="M:System.Data.Entity.DbSet`1.Create``1">
            <summary>
                Creates a new instance of an entity for the type of this set or for a type derived
                from the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxies and the entity type meets the requirements for creating a proxy.
            </summary>
            <typeparam name = "TDerivedEntity">The type of entity to create.</typeparam>
            <returns> The entity instance, which may be a proxy. </returns>
        </member>
        <member name="M:System.Data.Entity.DbSet`1.op_Implicit(System.Data.Entity.DbSet{`0})~System.Data.Entity.DbSet">
            <summary>
                Returns the equivalent non-generic <see cref="T:System.Data.Entity.DbSet"/> object.
            </summary>
            <returns>The non-generic set object.</returns>
        </member>
        <member name="M:Sy