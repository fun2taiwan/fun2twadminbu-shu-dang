ntity.Migrations.Model.AddColumnOperation"/>.
            </summary>
            <param name="addColumnOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.DropColumnOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.DropColumnOperation"/>.
            </summary>
            <param name="dropColumnOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.Al