ernal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndSummary">
            <summary>
                summary
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndTitle">
            <summary>
                title
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndContributorEmail">
            <summary>
                contributor/email
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndContributorName">
            <summary>
                contributor/name
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndContributorUri">
            <summary>
                contributor/uri
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndCategoryLabel">
            <summary>
                category/@label
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Serialization.Xml.Internal.Csdl.EdmModelCsdlSchemaWriter.XmlConstants.SyndContentKindPlaintext">
            <summary>
                Plaintext
            </summary>
        </member>
        <member name="F