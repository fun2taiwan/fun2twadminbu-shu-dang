 name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.DbMigration.RenameTable(System.String,System.String,System.Object)">
            <summary>
                Adds an operation to rename a table. To change the schema of a table use MoveTable
            </summary>
            <param name = "name">
                The name of the table to be renamed.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "newName">
                The new name for the table.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "anonymousArguments">
                Additional arguments that may be p