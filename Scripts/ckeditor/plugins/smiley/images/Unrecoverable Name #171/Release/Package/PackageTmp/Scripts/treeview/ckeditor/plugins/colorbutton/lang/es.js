table.
                The migrations history table is used to store a log of the migrations that have been applied to the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.HistoryOperation.#ctor(System.String,System.String,System.Object)">
            <summary>
                Initializes a new instance of the HistoryOperation class.
            </summary>
            <param name = "table">Name of the migrations history table.</param>
            <param name = "migrationId">Name of the migration being affected.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.HistoryOperation.Table">
           