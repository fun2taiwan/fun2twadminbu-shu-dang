se information.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger">
            <summary>
                Base class for loggers that can be used for the migrations process.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger.Info(System.String)">
            <summary>
                Logs an informational message.
            </summary>
            <param name = "message">The message to be logged.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger.Warning(System.String)">
            <summary>
                Logs a warning that the user should be made aware of.
            </summary>
            <param name = "message">The message to be logged.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger.Verbose(System.String)">
            <summary>
                Logs some additional information that should only be presented to the user if they request verbose output.
            </summary>
            <param name = "message">The message to be logged.</param>
        </member>
        <member name="T:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator">
            <summary>
                Generates VB.Net code for a code-based migration.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.String,System.Collections.Generic.IEnumerable{System.Data.Entity.Migrations.Model.MigrationOperation},System.String,System.String,System.String,System.String)">
            <inheritdoc />
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Collections.Generic.IEnumerable{System.Data.Entity.Migrations.Model.MigrationOperation},System.String,System.String)">
            <summary>
                Generates the primary code file that the user can view and edit